import { postApiResponse } from './apiUtils';

export const registerUser = async (registerUserBody) => {
  const registerUserResponse = postApiResponse({
    url: '/auth/register',
    body: registerUserBody,
  });
  return registerUserResponse;
};

export const loginUser = async (loginBody) => {
  const loginUserResponse = await postApiResponse({
    url: '/auth/login',
    body: loginBody,
  });
  return loginUserResponse;
};
