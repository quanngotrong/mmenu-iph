import { uploadImageResponse } from './apiUtils';

export const uploadInvoiceImage = async ({ formData, mallId, storeId }) => {
  const uploadInvoiceImageResponse = await uploadImageResponse({
    formData,
    url: `/malls/${mallId}/stores/${storeId}/invoice/upload-image`,
  });
  return uploadInvoiceImageResponse;
};
