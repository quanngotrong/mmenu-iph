import axios from 'axios';
import _ from 'lodash';

import {
  getPreferredLanguage,
  isWeb,
  getJWTToken,
} from '../service/localStorage';

const baseApi = 'http://o35zc9swl5.execute-api.us-east-1.amazonaws.com/v1';
export const baseUrl = `${baseApi}/v1`;
export const baseUrlV2 = `${baseApi}/v2`;

// TODO: server
const _getLang = () => {
  if (isWeb()) {
    return getPreferredLanguage();
  }
  return 'en';
};

export const getApiResponse = async ({
  url,
  token,
  queryParams,
  version = 'v1',
  lang,
}) => {
  const option = {
    headers: {
      'Content-Type': 'application/json',
      appid: 'mmenu-mall-customer',
      lang: lang || _getLang(),
    },
  };

  if (token || getJWTToken()) {
    _.set(option, 'headers.authorization', `Bearer ${token || getJWTToken()}`);
  }
  let query;
  if (queryParams) {
    const queryStr = Object.keys(queryParams)
      .map(
        (k) => `${encodeURIComponent(k)}=${encodeURIComponent(queryParams[k])}`
      )
      .join('&');
    query = `?${queryStr}`;
  }
  let baseUrlWithVersion = baseUrl;
  if (version === 'v2') {
    baseUrlWithVersion = baseUrl.replace(/v1/, 'v2');
  }
  const urlWithBase = [`${baseUrlWithVersion}${url}`, query].join('');
  const fetchResult = await axios.get(urlWithBase, option);
  const result = await fetchResult.data;
  return result;
};

export const postApiResponse = async ({
  url,
  body,
  token,
  lang,
  version = 'v1',
}) => {
  const option = {
    headers: {
      'Content-Type': 'application/json',
      appid: 'mmenu-mall-customer',
      lang: lang || _getLang(),
    },
  };

  if (token || getJWTToken()) {
    _.set(option, 'headers.authorization', `Bearer ${token || getJWTToken()}`);
  }
  let baseUrlWithVersion = baseUrl;
  if (version === 'v2') {
    baseUrlWithVersion = baseUrl.replace(/v1/, 'v2');
  }
  const urlWithBase = `${baseUrlWithVersion}${url}`;
  const fetchResult = await axios.post(urlWithBase, body, option);
  const result = await fetchResult.data;
  return result;
};

export const patchApiResponse = async ({
  url,
  body,
  token,
  lang,
  version = 'v1',
}) => {
  const option = {
    headers: {
      'Content-Type': 'application/json',
      appid: 'mmenu-mall-customer',
      lang: lang || _getLang(),
    },
  };
  if (token || getJWTToken()) {
    _.set(option, 'headers.authorization', `Bearer ${token || getJWTToken()}`);
  }

  let baseUrlWithVersion = baseUrl;
  if (version === 'v2') {
    baseUrlWithVersion = baseUrl.replace(/v1/, 'v2');
  }
  const urlWithBase = `${baseUrlWithVersion}${url}`;
  const fetchResult = await axios.patch(urlWithBase, body, option);
  const result = await fetchResult.data;
  return result;
};

export const uploadImageResponse = async ({
  url,
  formData,
  token,
  version = 'v1',
}) => {
  const option = {
    headers: {
      'Content-Type': 'multipart/form-data',
      appid: 'mmenu-mall-customer',
    },
  };

  if (token || getJWTToken()) {
    _.set(option, 'headers.authorization', `Bearer ${token || getJWTToken()}`);
  }

  let baseUrlWithVersion = baseUrl;
  if (version === 'v2') {
    baseUrlWithVersion = baseUrl.replace(/v1/, 'v2');
  }
  const urlWithBase = `${baseUrlWithVersion}${url}`;
  const fetchResult = await axios.post(urlWithBase, formData, option);
  const result = await fetchResult.data;
  return result;
};
