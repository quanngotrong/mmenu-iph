import _ from 'lodash';

export const isServer = () => typeof window === `undefined`;
export const isWeb = () => !isServer();

const _defaultLang = 'en';
const localStorageCache = {};

const _setLocalStorage = ({ key, val }) => {
  if (!isServer()) {
    localStorageCache[key] = val;
    window.localStorage.setItem(key, val);
  }
};

const _setJsonDataToLocalStorage = ({ key, val }) => {
  if (isServer()) {
    return null;
  }
  return _setLocalStorage({ key, val: JSON.stringify(val) });
};

// set du lieu vao local storage
const _getLocalStorage = ({ key }) => {
  if (isServer()) {
    return null;
  }
  if (localStorageCache[key]) {
    return localStorageCache[key];
  }
  const localStorageVal = window.localStorage.getItem(key);
  if (localStorageVal) {
    localStorageCache[key] = localStorageVal;
  }
  return localStorageVal;
};

// lay du lieu json tu localstorage. Neu co loi tra ra null
const _getLocalStorageJsondata = ({ key }) => {
  try {
    const localStorageString = _getLocalStorage({ key });
    if (localStorageString) {
      return JSON.parse(localStorageString);
    }
  } catch (err) {
    return null;
  }
  return null;
};

// copy tu web cu, do luu user o dang dac biet
const _parseObjectFromLocalStorage = (key) => {
  if (isServer()) {
    return null;
  }
  return JSON.parse(JSON.parse(_getLocalStorage({ key })));
};

// copy tu web cu, do luu user o dang dac biet
const _saveObjectToLocalStorage = (data, key) => {
  if (!isServer()) {
    _setLocalStorage({ key, val: JSON.stringify(JSON.stringify(data)) });
  }
};

function _saveStringToLocalStorage(data, key) {
  _setLocalStorage({ key, val: JSON.stringify(data) });
}

function _parseStringFromLocalStorage(key) {
  if (isServer()) {
    return null;
  }
  return JSON.parse(_getLocalStorage({ key }));
}

export const setRestaurantInfo = (restaurant) => {
  _setJsonDataToLocalStorage({ key: 'restaurantInfo', data: restaurant });
};

export const getRestaurantInfo = () => {
  return _getLocalStorageJsondata({ key: 'restaurantInfo' });
};

export const setUserInfo = (user) => {
  _saveObjectToLocalStorage(user, 'flutter.USER');
};

export const getUserInfo = () => {
  return _parseObjectFromLocalStorage('flutter.USER');
};

// set timestamp, restaurantId, tableId for Qr scan
export const setRestaurantIdAndTableIdForQrOrder = ({
  restaurantId,
  tableId,
  restaurantInfo,
}) => {
  const dateEpochTime = new Date().getTime();
  _setLocalStorage({ key: 'restaurantId', val: restaurantId });
  _setLocalStorage({ key: 'tableId', val: tableId });
  _setLocalStorage({ key: 'qrTimestamp', val: dateEpochTime });
  _setJsonDataToLocalStorage({ key: 'restaurantInfo', val: restaurantInfo });
};

export const setUserLoginLine = ({ restaurantId }) => {
  _setLocalStorage({ key: 'userLoginLine', val: restaurantId });
};

export const setUserLoginZalo = ({ restaurantId }) => {
  _setLocalStorage({ key: 'userLoginZalo', val: restaurantId });
};

export const isUserLoginZalo = ({ restaurantId }) => {
  return _getLocalStorage({ key: 'userLoginZalo' }) === restaurantId;
};

export const hasUser = () => {
  const user = getUserInfo();
  return !_.isEmpty(user);
};

export const isJapaneseRestaurant = () => {
  const restaurantInfo = _getLocalStorageJsondata({ key: 'restaurantInfo' });
  const country = _.get(restaurantInfo, 'country');
  return _.toLower(country) === 'japan';
};

export const isThaiRestaurant = () => {
  const restaurantInfo = _getLocalStorageJsondata({ key: 'restaurantInfo' });
  const country = _.get(restaurantInfo, 'country');
  return _.toLower(country) === 'thailand';
};

export const isVietnameseRestaurant = () => {
  const restaurantInfo = _getLocalStorageJsondata({ key: 'restaurantInfo' });
  const country = _.get(restaurantInfo, 'country') || 'việt nam';
  return _.toLower(country) === 'việt nam';
};

export const getRestaurantAndTableId = () => {
  const restaurantId = _getLocalStorage({ key: 'restaurantId' });
  const tableId = _getLocalStorage({ key: 'tableId' });
  return { restaurantId, tableId };
};

export const setJWTToken = (token) => {
  _saveStringToLocalStorage(token, 'flutter.JWT_TOKEN');
};

export const getJWTToken = () => {
  return _parseStringFromLocalStorage('flutter.JWT_TOKEN');
};

export const setPreferredLanguage = (locale) => {
  _saveStringToLocalStorage(locale, 'flutter.LANG');
};

const _getBrowserLanguage = () => {
  if (isServer()) {
    return _defaultLang;
  }
  const navigatorLang = navigator.language || '';
  if (navigatorLang.includes('vi')) {
    return 'vi';
  }
  if (navigatorLang.includes('ja')) {
    return 'ja';
  }
  if (navigatorLang.includes('en')) {
    return 'en';
  }
  if (navigatorLang.includes('ko')) {
    return 'ko';
  }
  if (navigatorLang.includes('zh')) {
    return 'zh';
  }
  if (navigatorLang.includes('th')) {
    return 'th';
  }
  return _defaultLang;
};

export const getPreferredLanguage = () => {
  return _parseStringFromLocalStorage('flutter.LANG') || _getBrowserLanguage();
};

// check xem user co the order hay ko
// sau khi quet ma qr 3 tieng, ko cho order
export const canOrder = () => {
  const qrTime = _getLocalStorage({ key: 'qrTimestamp' }) || 0;
  const restaurantInfo =
    _getLocalStorageJsondata({ key: 'restaurantInfo' }) || {};
  const now = new Date().getTime();
  const result =
    now - qrTime < 5 * 3600 * 1000 && !_.get(restaurantInfo, 'doNotAllowOrder');
  return result;
};

export const setCardId = (cardId) => {
  _saveStringToLocalStorage(cardId, 'cardId');
};

export const getCardId = () => {
  return _parseStringFromLocalStorage('cardId');
};

export const clearLocalStorage = () => {
  if (!isServer()) {
    window.localStorage.clear();
  }
};
