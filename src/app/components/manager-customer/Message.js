import {
  Stack,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  TextField,
  InputAdornment,
  TableRow,
  TableContainer,
  Table,
  TableHead,
  TableCell,
  Pagination,
  TableBody,
} from '@mui/material';
import { Search } from '@mui/icons-material';
import styled from '@emotion/styled';
import React from 'react';
import MmenuButton from '../MmenuButton';
import Campaign from './Campaign';

const columns = [
  { id: 'serial', label: 'STT', minWidth: 50 },
  { id: 'id', label: 'Tên chiến dịch', minWidth: 120 },
  {
    id: 'number',
    label: 'Thời gian',
    minWidth: 170,
    align: 'right',
    format: (value) => value.toLocaleString('en-US'),
  },
  {
    id: 'time',
    label: 'Trạng thái',
    minWidth: 170,
    align: 'right',
    format: (value) => value.toLocaleString('en-US'),
  },
];

function createData(serial, id, number, time) {
  const density = number / time;
  return { serial, id, number, time, density };
}

const StyledTableRow = styled(TableRow)(() => ({
  // hide last border
  '&:last-child td, &:last-child th': {
    border: 2,
  },
}));

const rows = [
  createData('India', 'IN', 1324171354, 3287263),
  createData('China', 'CN', 1403500365, 9596961),
  createData('Italy', 'IT', 60483973, 301340),
  createData('United States', 'US', 327167434, 9833520),
  createData('Canada', 'CA', 37602103, 9984670),
  createData('Australia', 'AU', 25475400, 7692024),
  createData('Germany', 'DE', 83019200, 357578),
  createData('Ireland', 'IE', 4857000, 70273),
  createData('Mexico', 'MX', 126577691, 1972550),
  createData('Japan', 'JP', 126317000, 377973),
  createData('France', 'FR', 67022000, 640679),
  createData('United Kingdom', 'GB', 67545757, 242495),
  createData('Russia', 'RU', 146793744, 17098246),
  createData('Nigeria', 'NG', 200962417, 923768),
  createData('Brazil', 'BR', 210147125, 8515767),
];

const Message = () => {
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [createMessage, setCreateMessage] = React.useState(false);
  return (
    <>
      {!createMessage ? (
        <>
          <Stack direction={'row'} spacing={2} justifyContent={'space-between'}>
            <TextField
              id="outlined-basic"
              placeholder="Search"
              variant="outlined"
              sx={{
                width: 0.4,
              }}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <Search></Search>
                  </InputAdornment>
                ),
              }}
            />
            <FormControl
              sx={{
                width: 0.3,
                flex: 1,
              }}
            >
              <InputLabel id="demo-simple-select-label" sx={{ width: 1 }}>
                Trạng thái
              </InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                label="Age"
              >
                <MenuItem value={'male'}>Đã hủy</MenuItem>
                <MenuItem value={'female'}>Đã kết thúc</MenuItem>
                <MenuItem value={'orther'}>Chưa bắt đầu</MenuItem>
                <MenuItem value={'orther'}>Đang diễn ra</MenuItem>
                <MenuItem value={'orther'}>Tạm dừng</MenuItem>
              </Select>
            </FormControl>
            <Stack width={0.3}>
              <MmenuButton onClick={() => setCreateMessage(true)}>
                Tạo chiến dịch
              </MmenuButton>
            </Stack>
          </Stack>
          <TableContainer sx={{ maxHeight: 440 }}>
            <Table stickyHeader aria-label="sticky table">
              <TableHead>
                <StyledTableRow>
                  {columns.map((column) => (
                    <TableCell
                      key={column.id}
                      align={column.align}
                      style={{ minWidth: column.minWidth }}
                    >
                      {column.label}
                    </TableCell>
                  ))}
                </StyledTableRow>
              </TableHead>
              <TableBody>
                {rows
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row) => {
                    return (
                      <StyledTableRow
                        hover
                        role="checkbox"
                        tabIndex={-1}
                        key={row.code}
                        sx={{
                          border: 1,
                        }}
                        onClick={() => handleOpenUpdate()}
                      >
                        {columns.map((column) => {
                          const value = row[column.id];
                          return (
                            <TableCell key={column.id} align={column.align}>
                              {column.format && typeof value === 'number'
                                ? column.format(value)
                                : value}
                            </TableCell>
                          );
                        })}
                      </StyledTableRow>
                    );
                  })}
              </TableBody>
            </Table>
          </TableContainer>
          <Stack alignItems={'center'} mt={'50px'}>
            <Pagination count={10}></Pagination>
          </Stack>
        </>
      ) : (
        <Campaign onClose={() => setCreateMessage(false)}></Campaign>
      )}
    </>
  );
};

export default Message;
