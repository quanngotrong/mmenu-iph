import {
  InputAdornment,
  TextField,
  Stack,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  TableContainer,
  Table,
  TableHead,
  TableCell,
  TableBody,
  TableRow,
  Pagination,
} from '@mui/material';
import React from 'react';
import { Search } from '@mui/icons-material';
import styled from '@emotion/styled';
import MmenuButton from '../MmenuButton';
import CustomerInfor from './CustomerInfor';

const columns = [
  { id: 'serial', label: 'STT', minWidth: 50 },
  { id: 'id', label: 'Thời gian', minWidth: 120 },
  {
    id: 'number',
    label: 'Hóa đơn',
    minWidth: 170,
    align: 'right',
    format: (value) => value.toLocaleString('en-US'),
  },
  {
    id: 'time',
    label: 'Tổng số tiền',
    minWidth: 170,
    align: 'right',
    format: (value) => value.toLocaleString('en-US'),
  },
  {
    id: 'total',
    label: 'Mã Voucher',
    minWidth: 170,
    align: 'right',
    format: (value) => value.toFixed(2),
  },
  {
    id: 'dícount',
    label: 'Số tiền giảm giá',
    minWidth: 170,
    align: 'right',
    format: (value) => value.toFixed(2),
  },
  {
    id: 'dícount',
    label: 'Cửa hàng',
    minWidth: 170,
    align: 'right',
    format: (value) => value.toFixed(2),
  },
];

function createData(serial, id, number, time) {
  const density = number / time;
  return { serial, id, number, time, density };
}

const StyledTableRow = styled(TableRow)(() => ({
  // hide last border
  '&:last-child td, &:last-child th': {
    border: 2,
  },
}));

const rows = [
  createData('India', 'IN', 1324171354, 3287263),
  createData('China', 'CN', 1403500365, 9596961),
  createData('Italy', 'IT', 60483973, 301340),
  createData('United States', 'US', 327167434, 9833520),
  createData('Canada', 'CA', 37602103, 9984670),
  createData('Australia', 'AU', 25475400, 7692024),
  createData('Germany', 'DE', 83019200, 357578),
  createData('Ireland', 'IE', 4857000, 70273),
  createData('Mexico', 'MX', 126577691, 1972550),
  createData('Japan', 'JP', 126317000, 377973),
  createData('France', 'FR', 67022000, 640679),
  createData('United Kingdom', 'GB', 67545757, 242495),
  createData('Russia', 'RU', 146793744, 17098246),
  createData('Nigeria', 'NG', 200962417, 923768),
  createData('Brazil', 'BR', 210147125, 8515767),
];

const Customer = () => {
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [updatePage, setUpdatePage] = React.useState(false);

  const handleOpenUpdate = () => {
    setUpdatePage(true);
  };
  return (
    <Stack spacing={2}>
      {!updatePage ? (
        <>
          <Stack direction={'row'} spacing={2}>
            <Stack width={1} spacing={2}>
              <TextField
                id="outlined-basic"
                placeholder="Search"
                variant="outlined"
                sx={{
                  width: 1,
                }}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <Search></Search>
                    </InputAdornment>
                  ),
                }}
              />
              <FormControl
                sx={{
                  width: 1,
                  flex: 1,
                }}
              >
                <InputLabel id="demo-simple-select-label">Thời gian</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  label="Age"
                >
                  <MenuItem value={'male'}>Tháng một</MenuItem>
                  <MenuItem value={'female'}>Tháng hai</MenuItem>
                  <MenuItem value={'orther'}>Khác</MenuItem>
                </Select>
              </FormControl>
            </Stack>
            <Stack width={1} spacing={2}>
              <FormControl
                sx={{
                  width: 1,
                }}
              >
                <InputLabel id="demo-simple-select-label">Cửa hàng</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  label="Age"
                >
                  <MenuItem value={'male'}>Tất cả gian hàng</MenuItem>
                  <MenuItem value={'female'}>Nữ</MenuItem>
                  <MenuItem value={'orther'}>Khác</MenuItem>
                </Select>
              </FormControl>
              <FormControl
                sx={{
                  width: 1,
                  flex: 1,
                }}
              >
                <InputLabel id="demo-simple-select-label">Khác</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  label="Age"
                >
                  <MenuItem value={'male'}>Khác</MenuItem>
                  <MenuItem value={'female'}>Nữ</MenuItem>
                  <MenuItem value={'orther'}>Nam</MenuItem>
                </Select>
              </FormControl>
            </Stack>
            <Stack width={1} spacing={2}>
              <MmenuButton>Gửi tin nhắn</MmenuButton>
              <MmenuButton
                fullWidth
                sx={{
                  backgroundColor: 'white !important',
                  color: 'black',
                  border: '1px solid #E2E2E2',
                }}
              >
                Xuất
              </MmenuButton>
            </Stack>
          </Stack>
          <TableContainer sx={{ maxHeight: 440 }}>
            <Table stickyHeader aria-label="sticky table">
              <TableHead>
                <StyledTableRow>
                  {columns.map((column) => (
                    <TableCell
                      key={column.id}
                      align={column.align}
                      style={{ minWidth: column.minWidth }}
                    >
                      {column.label}
                    </TableCell>
                  ))}
                </StyledTableRow>
              </TableHead>
              <TableBody>
                {rows
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row) => {
                    return (
                      <StyledTableRow
                        hover
                        role="checkbox"
                        tabIndex={-1}
                        key={row.code}
                        sx={{
                          border: 1,
                        }}
                        onClick={() => handleOpenUpdate()}
                      >
                        {columns.map((column) => {
                          const value = row[column.id];
                          return (
                            <TableCell key={column.id} align={column.align}>
                              {column.format && typeof value === 'number'
                                ? column.format(value)
                                : value}
                            </TableCell>
                          );
                        })}
                      </StyledTableRow>
                    );
                  })}
              </TableBody>
            </Table>
          </TableContainer>
          <Stack alignItems={'center'} mt={'50px'}>
            <Pagination count={10}></Pagination>
          </Stack>
        </>
      ) : (
        <CustomerInfor></CustomerInfor>
      )}
    </Stack>
  );
};

export default Customer;
