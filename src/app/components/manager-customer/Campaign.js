import { Stack, Typography, FormControlLabel, Checkbox } from '@mui/material';
import CustomizedField from '../CustomerField';
import MmenuButton from '../MmenuButton';

const Campaign = ({ onClose }) => {
  return (
    <Stack width={0.8} pb={12}>
      <Typography variant="h1" fontSize={20} fontWeight={700} mb={2}>
        Tạo mới chiến dịch
      </Typography>
      <Typography>Send to</Typography>
      <Stack direction={'row'} alignItems={'center'} position={'relative'}>
        <Typography>
          Khách hàng có sinh nhật trong tháng: Tháng 1, Tháng 2, Tháng 3, Tháng
          4
        </Typography>
        <Typography>(1.234 khách)</Typography>
        <Stack width={0.2} position={'absolute'} right={'-23%'}>
          <MmenuButton>Chọn lại</MmenuButton>
        </Stack>
      </Stack>
      <Typography
        textAlign={'end'}
        color={'#3D2EE0'}
        sx={{ cursor: 'pointer' }}
      >
        Tải xuống danh sách
      </Typography>
      <Typography>Send via</Typography>
      <FormControlLabel control={<Checkbox defaultChecked />} label="Zalo" />
      <CustomizedField required label={'Campaign name'}></CustomizedField>
      <CustomizedField label={'Mô tả'}></CustomizedField>
      <CustomizedField required label={'Thời gian gửi'}></CustomizedField>
      <CustomizedField
        label={'Nội dung tin nhắn'}
        multiline
        rows={4}
      ></CustomizedField>
      <Stack
        direction={'row'}
        boxShadow={'0px -2px 6px rgba(0, 0, 0, 0.1)'}
        p={2}
        position={'fixed'}
        width={'calc(100% + 32px)'}
        bgcolor={'white'}
        bottom={0}
        left={'-16px'}
        spacing={2}
        justifyContent={'flex-end'}
        pr={4}
      >
        <MmenuButton
          sx={{
            width: '15%',
            bgcolor: 'white !important',
            color: 'black',
            border: '1px solid #E2E2E2',
          }}
          onClick={onClose}
        >
          Hủy
        </MmenuButton>
        <MmenuButton sx={{ width: '15%' }}>Xác nhận</MmenuButton>
      </Stack>
    </Stack>
  );
};

export default Campaign;
