import { ArrowDropUp, ArrowDropDown } from '@mui/icons-material';
import {
  Stack,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Card,
  CardActionArea,
  CardContent,
  Typography,
} from '@mui/material';
import { useState } from 'react';
import classNames from 'classnames/bind';
import styles from './ManagerCustomer.module.css';

const cx = classNames.bind(styles);

const OverView = () => {
  const [time, setTime] = useState('');

  const handleChange = (event) => {
    setTime(event.target.value);
  };
  const getGrowthIcon = (perCent = 0) => {
    if (perCent >= 0) {
      return (
        <Stack
          direction={'row'}
          position={'absolute'}
          top={0}
          right={0}
          bgcolor={'#09B331'}
          color={'white'}
          p={'5px 10px'}
          borderRadius={'0px 0px 0px 20px'}
        >
          <ArrowDropUp></ArrowDropUp>
          <Typography>{perCent}%</Typography>
        </Stack>
      );
    }
    return (
      <Stack
        direction={'row'}
        position={'absolute'}
        top={0}
        right={0}
        bgcolor={'#FF4151'}
        color={'white'}
        p={'5px 10px'}
        borderRadius={'0px 0px 0px 20px'}
      >
        <ArrowDropDown></ArrowDropDown>
        <Typography>{Math.abs(perCent)}%</Typography>
      </Stack>
    );
  };
  return (
    <Stack mr={2}>
      <FormControl sx={{ width: '300px' }}>
        <InputLabel id="demo-simple-select-label">Thời gian</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={time}
          label="Age"
          onChange={handleChange}
        >
          <MenuItem value={10}>Hôm nay</MenuItem>
          <MenuItem value={20}>Tuần nay</MenuItem>
          <MenuItem value={30}>Tháng nay</MenuItem>
        </Select>
      </FormControl>
      <Stack className={cx('banner-admin')}>
        <Typography>All customers</Typography>
        <Typography variant="h4" fontWeight={700} fontSize={32}>
          10.000
        </Typography>
      </Stack>
      <Stack
        width={1}
        mt={2}
        direction={'row'}
        display={'grid'}
        gridTemplateColumns={'calc(100% / 3) calc(100% / 3) calc(100% / 3)'}
        rowGap={'10px'}
        columnGap={'10px'}
        justifyContent={'space-between'}
      >
        <Card
          sx={{
            maxWidth: 345,
            borderRadius: '8px',
            boxShadow: 'none',
            border: '1px solid #D5D9E2',
            position: 'relative',
          }}
        >
          <CardActionArea>
            <CardContent>
              <Typography variant="body2" color="text.secondary">
                Revenue
              </Typography>
              <Typography gutterBottom variant="h5" component="div" mt={2}>
                10.000.000.000
              </Typography>
            </CardContent>
          </CardActionArea>
          {getGrowthIcon(-5)}
        </Card>
        <Card
          sx={{
            maxWidth: 345,
            borderRadius: '8px',
            boxShadow: 'none',
            border: '1px solid #D5D9E2',
            position: 'relative',
          }}
        >
          <CardActionArea>
            <CardContent>
              <Typography variant="body2" color="text.secondary">
                Revenue
              </Typography>
              <Typography gutterBottom variant="h5" component="div">
                10.000.000.000
              </Typography>
            </CardContent>
          </CardActionArea>
          {getGrowthIcon(10)}
        </Card>
        <Card
          sx={{
            maxWidth: 345,
            borderRadius: '8px',
            boxShadow: 'none',
            border: '1px solid #D5D9E2',
            position: 'relative',
          }}
        >
          <CardActionArea>
            <CardContent>
              <Typography variant="body2" color="text.secondary">
                Revenue
              </Typography>
              <Typography gutterBottom variant="h5" component="div">
                10.000.000.000
              </Typography>
            </CardContent>
          </CardActionArea>
          {getGrowthIcon(-5)}
        </Card>
        <Card
          sx={{
            maxWidth: 345,
            borderRadius: '8px',
            boxShadow: 'none',
            border: '1px solid #D5D9E2',
            position: 'relative',
          }}
        >
          <CardActionArea>
            <CardContent>
              <Typography variant="body2" color="text.secondary">
                Revenue
              </Typography>
              <Typography gutterBottom variant="h5" component="div">
                10.000.000.000
              </Typography>
            </CardContent>
          </CardActionArea>
          {getGrowthIcon(-5)}
        </Card>
        <Card
          sx={{
            maxWidth: 345,
            borderRadius: '8px',
            boxShadow: 'none',
            border: '1px solid #D5D9E2',
            position: 'relative',
          }}
        >
          <CardActionArea>
            <CardContent>
              <Typography variant="body2" color="text.secondary">
                Revenue
              </Typography>
              <Typography gutterBottom variant="h5" component="div">
                10.000.000.000
              </Typography>
            </CardContent>
          </CardActionArea>
          {getGrowthIcon(-5)}
        </Card>
        <Card
          sx={{
            maxWidth: 345,
            borderRadius: '8px',
            boxShadow: 'none',
            border: '1px solid #D5D9E2',
            position: 'relative',
          }}
        >
          <CardActionArea>
            <CardContent>
              <Typography variant="body2" color="text.secondary">
                Revenue
              </Typography>
              <Typography gutterBottom variant="h5" component="div">
                10.000.000.000
              </Typography>
            </CardContent>
          </CardActionArea>
          {getGrowthIcon(-5)}
        </Card>
      </Stack>
    </Stack>
  );
};

export default OverView;
