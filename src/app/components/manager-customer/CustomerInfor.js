import {
  Typography,
  Stack,
  TableRow,
  TableContainer,
  TableHead,
  Table,
  TableCell,
  TableBody,
  Pagination,
} from '@mui/material';
import React from 'react';
import styled from '@emotion/styled';
import CustomizedField from '../CustomerField';
import MmenuButton from '../MmenuButton';
import MmenuWhiteButton from '../MmenuWhiteButton';

const columns = [
  { id: 'serial', label: 'STT', minWidth: 50 },
  { id: 'id', label: 'Thời gian', minWidth: 120 },
  {
    id: 'number',
    label: 'Hóa đơn',
    minWidth: 170,
    align: 'right',
    format: (value) => value.toLocaleString('en-US'),
  },
  {
    id: 'time',
    label: 'Tổng số tiền',
    minWidth: 170,
    align: 'right',
    format: (value) => value.toLocaleString('en-US'),
  },
  {
    id: 'total',
    label: 'Mã Voucher',
    minWidth: 170,
    align: 'right',
    format: (value) => value.toFixed(2),
  },
  {
    id: 'dícount',
    label: 'Số tiền giảm giá',
    minWidth: 170,
    align: 'right',
    format: (value) => value.toFixed(2),
  },
  {
    id: 'dícount',
    label: 'Cửa hàng',
    minWidth: 170,
    align: 'right',
    format: (value) => value.toFixed(2),
  },
];

function createData(serial, id, number, time) {
  const density = number / time;
  return { serial, id, number, time, density };
}

const StyledTableRow = styled(TableRow)(() => ({
  // hide last border
  '&:last-child td, &:last-child th': {
    border: 2,
  },
}));

const rows = [
  createData('India', 'IN', 1324171354, 3287263),
  createData('China', 'CN', 1403500365, 9596961),
  createData('Italy', 'IT', 60483973, 301340),
  createData('United States', 'US', 327167434, 9833520),
  createData('Canada', 'CA', 37602103, 9984670),
  createData('Australia', 'AU', 25475400, 7692024),
  createData('Germany', 'DE', 83019200, 357578),
  createData('Ireland', 'IE', 4857000, 70273),
  createData('Mexico', 'MX', 126577691, 1972550),
  createData('Japan', 'JP', 126317000, 377973),
  createData('France', 'FR', 67022000, 640679),
  createData('United Kingdom', 'GB', 67545757, 242495),
  createData('Russia', 'RU', 146793744, 17098246),
  createData('Nigeria', 'NG', 200962417, 923768),
  createData('Brazil', 'BR', 210147125, 8515767),
];

const CustomerInfor = () => {
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  return (
    <Stack>
      <Typography>THÔNG TIN KHÁCH HÀNG</Typography>
      <Stack direction={'row'} spacing={2} width={1}>
        <Stack width={1}>
          <CustomizedField
            label="Họ và tên"
            sx={{ marginBottom: 0 }}
          ></CustomizedField>
          <CustomizedField
            label="Số điện thoại"
            sx={{ marginBottom: 0 }}
          ></CustomizedField>
          <CustomizedField
            label="Ngày sinh"
            sx={{ marginBottom: 0 }}
          ></CustomizedField>
          <CustomizedField
            label="Email"
            sx={{ marginBottom: 0 }}
          ></CustomizedField>
        </Stack>
        <Stack width={1}>
          <CustomizedField
            label="Zalo"
            sx={{ marginBottom: 0 }}
          ></CustomizedField>
          <CustomizedField
            label="Zalo"
            sx={{ marginBottom: 0 }}
          ></CustomizedField>
          <CustomizedField
            label="Ghi chú"
            sx={{ marginBottom: 0 }}
          ></CustomizedField>
        </Stack>
        <Stack width={1} spacing={1} pt={'8px'}>
          <MmenuButton>Sửa thông tin</MmenuButton>
          <MmenuWhiteButton title={'Cập nhật'}></MmenuWhiteButton>
        </Stack>
      </Stack>
      <TableContainer sx={{ maxHeight: 440 }}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <StyledTableRow>
              {columns.map((column) => (
                <TableCell
                  key={column.id}
                  align={column.align}
                  style={{ minWidth: column.minWidth }}
                >
                  {column.label}
                </TableCell>
              ))}
            </StyledTableRow>
          </TableHead>
          <TableBody>
            {rows
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((row) => {
                return (
                  <StyledTableRow
                    hover
                    role="checkbox"
                    tabIndex={-1}
                    key={row.code}
                    sx={{
                      border: 1,
                    }}
                  >
                    {columns.map((column) => {
                      const value = row[column.id];
                      return (
                        <TableCell key={column.id} align={column.align}>
                          {column.format && typeof value === 'number'
                            ? column.format(value)
                            : value}
                        </TableCell>
                      );
                    })}
                  </StyledTableRow>
                );
              })}
          </TableBody>
        </Table>
      </TableContainer>
      <Stack
        direction={'row'}
        bgcolor={'rgba(0, 0, 0, 0.15)'}
        height={48}
        alignItems={'center'}
        p={2}
      >
        <Typography width={340} variant="h1" fontSize={16} fontWeight={700}>
          Tổng số tiền
        </Typography>
        <Typography variant="h1" fontSize={16} fontWeight={700}>
          10.000.000
        </Typography>
        <Typography
          paddingLeft={10}
          width={340}
          variant="h1"
          fontSize={16}
          fontWeight={700}
        >
          Mã Voucher
        </Typography>
        <Typography variant="h1" fontSize={16} fontWeight={700}>
          10.000.000
        </Typography>
      </Stack>
      <Stack alignItems={'center'} mt={'50px'}>
        <Pagination count={10}></Pagination>
      </Stack>
    </Stack>
  );
};
export default CustomerInfor;
