import Image from 'next/image';
import { Stack, Typography } from '@mui/material';

const ErrorComponents = () => {
  return (
    <Stack
      alignItems={'center'}
      justifyContent={'center'}
      height={'100vh'}
      spacing={2}
      backgroundColor={'white'}
    >
      <Image src="/404-image.svg" width={300} height={300} alt=""></Image>
      <Typography textAlign={'center'} variant="h7" color="black">
        Vui lòng quét lại mã QR bằng ứng dụng Zalo để tiếp tục!
      </Typography>
    </Stack>
  );
};

export default ErrorComponents;
