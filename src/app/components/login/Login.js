import Image from 'next/image';
import _ from 'lodash';
import {
  Stack,
  Typography,
  InputAdornment,
  InputBase,
  IconButton,
  Grid,
} from '@mui/material';
import AccountCircleOutlinedIcon from '@mui/icons-material/AccountCircleOutlined';
import React from 'react';
import {
  VisibilityOffOutlined,
  VisibilityOutlined,
  LockOpenOutlined,
} from '@mui/icons-material';
import classNames from 'classnames/bind';
import { MallContext } from '@/app/state-manager/mallContext';
import MmenuButton from '../MmenuButton';
import styles from './Login.module.css';

const cx = classNames.bind(styles);

const LoginComponent = () => {
  const { dispatch } = React.useContext(MallContext);
  const [showPassword, setShowPassword] = React.useState(false);
  const [loginBody, setLoginBody] = React.useState({
    phone: '',
    password: '',
  });

  const handleUpdateLoginBody = ({ key, value }) => {
    if (_.hasIn(loginBody, key)) {
      setLoginBody({ ...loginBody, [key]: value });
    }
  };

  const handleSubmitLoginBody = () => {
    dispatch({
      type: 'LOGIN',
      payload: {
        loginUserBody: loginBody,
        dispatch,
      },
    });
  };

  const handleClickShowPassword = () => setShowPassword((show) => !show);
  return (
    <Grid container>
      <Grid item xl={4} sm={4} xs={12}>
        <Stack
          justifyContent={'space-between'}
          height={'100vh'}
          bgcolor={'#fff'}
          color={'#000'}
        >
          <Stack
            alignItems={'center'}
            paddingTop={'80px'}
            paddingLeft={'28px'}
            paddingRight={'28px'}
            spacing={2}
          >
            <Image src={'/iph-logo.svg'} width={200} height={60} alt=""></Image>
            <Typography variant="h6" fontWeight={600} padding={5}>
              Đăng nhập
            </Typography>
            <InputBase
              placeholder="Số điện thoại"
              sx={{
                width: 1,
                padding: '10px',
                border: '1px solid #E2E2E2',
                borderRadius: '12px',
              }}
              startAdornment={
                <InputAdornment position="start">
                  <AccountCircleOutlinedIcon />
                </InputAdornment>
              }
              onChange={(e) =>
                handleUpdateLoginBody({
                  key: 'phone',
                  value: e.target.value,
                })
              }
            ></InputBase>
            <InputBase
              placeholder="Mật khẩu"
              type={showPassword ? 'text' : 'password'}
              sx={{
                width: 1,
                padding: '10px',
                border: '1px solid #E2E2E2',
                borderRadius: '12px',
              }}
              startAdornment={
                <InputAdornment position="start">
                  <LockOpenOutlined />
                </InputAdornment>
              }
              onChange={(e) =>
                handleUpdateLoginBody({
                  key: 'password',
                  value: e.target.value,
                })
              }
              endAdornment={
                <InputAdornment position="end">
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={handleClickShowPassword}
                    edge="end"
                  >
                    {showPassword ? (
                      <VisibilityOffOutlined />
                    ) : (
                      <VisibilityOutlined />
                    )}
                  </IconButton>
                </InputAdornment>
              }
            ></InputBase>
            <Stack width={1} alignItems={'flex-end'}>
              <Typography>Quên mật khẩu</Typography>
            </Stack>
            <MmenuButton onClick={handleSubmitLoginBody}>Tiếp theo</MmenuButton>
          </Stack>
          <Stack
            direction={'row'}
            justifyContent={'space-between'}
            width={1}
            padding={'16px'}
          >
            <Typography variant="caption" fontWeight={600}>
              Indochina Plaza Hanoi<br></br> 241 Xuân Thủy, Cầu Giấy, Hà Nội
            </Typography>
            <Image
              src={'/power-by-mmenu.svg'}
              width={86}
              height={28}
              alt=""
            ></Image>
          </Stack>
        </Stack>
      </Grid>
      <Grid item xl={8} sm={8} xs={0}>
        <Stack
          height={'100vh'}
          width={'100%'}
          className={cx('background-image-large')}
        ></Stack>
      </Grid>
    </Grid>
  );
};
export default LoginComponent;
