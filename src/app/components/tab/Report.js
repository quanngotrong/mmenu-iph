import {
  Stack,
  InputAdornment,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Typography,
  Pagination,
} from '@mui/material';
import SearchIcon from '@mui/icons-material/Search';
import { TimelineDot } from '@mui/lab';
import CustomizedField from '../CustomerField';
import MmenuButton from '../MmenuButton';

export default function Report() {
  return (
    <Stack>
      <CustomizedField
        type="text"
        placeholder="Tìm kiếm"
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <SearchIcon />
            </InputAdornment>
          ),
        }}
      ></CustomizedField>
      <Stack direction={'row'} spacing={1}>
        <FormControl fullWidth>
          <InputLabel id="demo-simple-select-label">Thời gian</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            label="Age"
          >
            <MenuItem value={'male'}>Hôm qua</MenuItem>
            <MenuItem value={'female'}>Tuần qua</MenuItem>
            <MenuItem value={'orther'}>Tháng qua</MenuItem>
          </Select>
        </FormControl>
        <MmenuButton>Tải xuống</MmenuButton>
      </Stack>
      <Stack
        border={'1px solid #151522'}
        mt={2}
        mb={2}
        padding={1}
        borderRadius={'8px'}
      >
        <Stack
          direction={'row'}
          justifyContent={'space-between'}
          alignItems={'center'}
        >
          <Stack direction={'row'} sx={{ alignItems: 'center' }} spacing={1}>
            <TimelineDot color="success"></TimelineDot>
            <Typography>Số hóa đơn áp dụng giảm giá</Typography>
          </Stack>
          <Typography>100.000</Typography>
        </Stack>
        <Stack
          direction={'row'}
          justifyContent={'space-between'}
          alignItems={'center'}
        >
          <Stack direction={'row'} sx={{ alignItems: 'center' }} spacing={1}>
            <TimelineDot color="primary"></TimelineDot>
            <Typography>Số hóa đơn áp dụng giảm giá</Typography>
          </Stack>
          <Typography>100.000</Typography>
        </Stack>
        <Stack
          direction={'row'}
          justifyContent={'space-between'}
          alignItems={'center'}
        >
          <Stack direction={'row'} sx={{ alignItems: 'center' }} spacing={1}>
            <TimelineDot></TimelineDot>
            <Typography>Số hóa đơn áp dụng giảm giá</Typography>
          </Stack>
          <Typography>100.000</Typography>
        </Stack>
      </Stack>
      <Stack border={'1px solid #151522'} p={1}>
        <Stack direction={'row'} justifyContent={'space-between'}>
          <Typography>09:00 01/01/2023</Typography>
          <Typography>500.000đ</Typography>
        </Stack>
        <Stack direction={'row'} justifyContent={'space-between'}>
          <Typography>09:00 01/01/2023</Typography>
          <Typography>500.000đ</Typography>
        </Stack>
        <Stack direction={'row'} justifyContent={'space-between'}>
          <Typography>09:00 01/01/2023</Typography>
          <Typography>500.000đ</Typography>
        </Stack>
      </Stack>
      <Stack border={'1px solid #151522'} p={1}>
        <Stack direction={'row'} justifyContent={'space-between'}>
          <Typography>09:00 01/01/2023</Typography>
          <Typography>500.000đ</Typography>
        </Stack>
        <Stack direction={'row'} justifyContent={'space-between'}>
          <Typography>09:00 01/01/2023</Typography>
          <Typography>500.000đ</Typography>
        </Stack>
        <Stack direction={'row'} justifyContent={'space-between'}>
          <Typography>09:00 01/01/2023</Typography>
          <Typography>500.000đ</Typography>
        </Stack>
      </Stack>
      <Pagination count={10}></Pagination>
    </Stack>
  );
}
