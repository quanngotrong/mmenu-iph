import _ from 'lodash';
import { Stack, Button, Avatar, IconButton } from '@mui/material';
import { useRef, useState } from 'react';
import styled from '@emotion/styled';
import CustomizedField from '../CustomerField';
import MmenuButton from '../MmenuButton';
import CameraIcon from '../icons/cameraIcon';

const WhiteButton = styled(Button)({
  boxShadow: 'none',
  textTransform: 'none',
  fontWeight: 600,
  fontSize: '16px',
  padding: '1rem',
  width: 'fit-content',
  border: '1.5px solid',
  borderRadius: '12px',
  backgroundColor: '#ffffff',
  color: '#000000',
  alignItems: 'center',
  '&:hover': {
    backgroundColor: '#fbfbfb',
  },
  '&:active': {
    backgroundColor: '#fbfbfb',
  },
});

export default function CreateOrderInformation({ hidden }) {
  const inputRef = useRef(null);
  const [imageUrls, setImageUrls] = useState([]);
  const handleInputChange = (event) => {
    const { files } = event.target;
    const urls = [];
    // eslint-disable-next-line no-plusplus
    for (let i = 0; i < files.length; i++) {
      const file = files[i];
      const reader = new FileReader();
      reader.onload = () => {
        const imageUrl = reader.result;
        urls.push(imageUrl); // Lưu đường dẫn của ảnh vào mảng urls
        if (urls.length === files.length) {
          setImageUrls([...imageUrls, ...urls]); // Cập nhật state với các đường dẫn mới
        }
      };
      reader.readAsDataURL(file);
    }
  };
  return (
    <Stack hidden={hidden} spacing={2}>
      <CustomizedField
        type="text"
        label="Mã giảm giá"
        startAdornment={''}
        name="enterVoucher"
        InputProps={{
          readOnly: true,
        }}
        defaultValue={'MGG0123456 - 50.000đ'}
        sx={{
          margin: 0,
        }}
      ></CustomizedField>
      <CustomizedField
        type="text"
        label="Nhập số hoá đơn"
        startAdornment={''}
        name="enterVoucher"
      ></CustomizedField>
      <CustomizedField
        type="text"
        label="Nhập số tiền hoá đơn"
        startAdornment={''}
        name="enterVoucher"
      ></CustomizedField>
      <CustomizedField
        type="text"
        label="Số tiền sau giảm giá"
        startAdornment={''}
        name="enterVoucher"
        InputProps={{
          readOnly: true,
        }}
      ></CustomizedField>
      <WhiteButton variant="filled" startIcon={<CameraIcon />}>
        Tải ảnh hóa đơn
      </WhiteButton>
      <Stack direction={'row'} spacing={1}>
        {_.map(imageUrls, (url) => (
          <Avatar
            variant="rounded"
            sx={{
              width: 60,
              height: 60,
            }}
            src={url}
          ></Avatar>
        ))}
        <IconButton
          color="primary"
          aria-label="upload picture"
          sx={{
            margin: '0',
            padding: '0',
          }}
          component="label"
        >
          <input
            type="file"
            accept="image/*"
            onChange={handleInputChange}
            ref={inputRef}
            style={{ display: 'none' }}
          />
          <Avatar
            variant="rounded"
            sx={{
              width: 60,
              height: 60,
            }}
            src="/plus.svg"
          ></Avatar>
        </IconButton>
      </Stack>
      <MmenuButton>Gửi thông tin</MmenuButton>
    </Stack>
  );
}
