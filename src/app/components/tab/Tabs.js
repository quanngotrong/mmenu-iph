import * as React from 'react';
import classNames from 'classnames/bind';
import { Tab, Tabs, Box, Stack, Typography, Grid } from '@mui/material';
import { TabPanel } from '@mui/lab';
import TabContext from '@mui/lab/TabContext';
import Image from 'next/image';
import ApplyVoucher from './ApplyVoucher';
import Report from './Report';
import styles from './Tabs.module.css';
import ReportLarge from './ReportLarge';

const cx = classNames.bind(styles);

function CustomizeTabs() {
  const [value, setValue] = React.useState('1');

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <Box sx={{ width: '100%' }} flex={1}>
      <Grid container>
        <Stack className={cx('tabs--mobile--hidden')} width={1}>
          <Grid item xs={12} sm={12}>
            <TabContext value={value}>
              <Box sx={{ width: '100%', backgroundColor: '#fff' }}>
                <Tabs
                  value={value}
                  onChange={handleChange}
                  aria-label="secondary tabs example"
                  variant="fullWidth"
                  indicatorColor="transparent"
                  textColor="#000"
                  centered
                  sx={{
                    '.Mui-selected': {
                      backgroundColor: '#000',
                      color: '#fff',
                    },
                    borderBottom: '1px solid #E2E2E2',
                  }}
                >
                  <Tab value="1" label="Áp dụng voucher" />
                  <Tab value="2" label="Báo cáo" />
                </Tabs>
              </Box>
              <TabPanel value="1">
                <ApplyVoucher />
              </TabPanel>
              <TabPanel value="2">
                <Report></Report>
              </TabPanel>
            </TabContext>
          </Grid>
        </Stack>
        <Stack
          className={cx('tabs--tablet--hidden', 'tabs--tablet')}
          direction={'row'}
          width={1}
        >
          <Grid item xl={4} sm={4} xs={12}>
            <Stack
              height={'calc(100vh - 4.25rem)'}
              bgcolor={'#fff'}
              p={2}
              position={'relative'}
              borderRight={'1px solid #e2e2e2'}
            >
              <ApplyVoucher></ApplyVoucher>
              <Stack
                direction={'row'}
                justifyContent={'space-between'}
                width={'-webkit-fill-available'}
                padding={'16px'}
                paddingLeft={0}
                position={'absolute'}
                bottom={'0'}
              >
                <Typography variant="caption" fontWeight={600}>
                  Indochina Plaza Hanoi<br></br> 241 Xuân Thủy, Cầu Giấy, Hà Nội
                </Typography>
                <Image
                  src={'/power-by-mmenu.svg'}
                  width={86}
                  height={28}
                  alt=""
                ></Image>
              </Stack>
            </Stack>
          </Grid>
          <Grid item xl={8} sm={8} xs={12}>
            <Stack height={'calc(100vh - 4.25rem)'} width={'100%'}>
              <ReportLarge></ReportLarge>
            </Stack>
          </Grid>
        </Stack>
      </Grid>
    </Box>
  );
}

export default CustomizeTabs;
