import { Stack, Typography } from '@mui/material';
import { styled } from '@mui/material/styles';
import Button from '@mui/material/Button';
import classNames from 'classnames/bind';
import { useState } from 'react';
import jsQR from 'jsqr';
import CameraIcon from '../icons/cameraIcon';
import CustomizedField from '../CustomerField';
import styles from './ApplyVoucher.module.css';
import MmenuButton from '../MmenuButton';
import CreateOrderInformation from './CreateOrderInformation';

const cx = classNames.bind(styles);

const WhiteButton = styled(Button)({
  boxShadow: 'none',
  textTransform: 'none',
  fontWeight: 600,
  fontSize: '16px',
  padding: '1rem 0.5rem',
  width: '50%',
  border: '1.5px solid',
  borderRadius: '12px',
  backgroundColor: '#ffffff',
  color: '#000000',
  alignItems: 'center',
  '&:hover': {
    backgroundColor: '#fbfbfb',
  },
  '&:active': {
    backgroundColor: '#fbfbfb',
  },
});

export default function ApplyVoucher() {
  const [nextStep, setNextStep] = useState(false);
  const [voucherCode, setVoucherCode] = useState('');
  const handleFileChange = (event) => {
    const file = event.target.files[0];
    if (!file) {
      return;
    }
    const reader = new FileReader();
    reader.onload = () => {
      const img = new Image();
      img.onload = () => {
        const canvas = document.createElement('canvas');
        canvas.width = img.width;
        canvas.height = img.height;
        const context = canvas.getContext('2d');
        context.drawImage(img, 0, 0, img.width, img.height);
        const imageData = context.getImageData(0, 0, img.width, img.height);
        const code = jsQR(imageData.data, imageData.width, imageData.height);
        if (code) {
          setVoucherCode(code.data.split('http://')[1]);
          // console.log('Mã QR được tìm thấy:', code.data.split('http://')[1]);
        } else {
          // console.log('Không tìm thấy mã QR trong ảnh');
        }
      };
      img.src = reader.result;
    };
    reader.readAsDataURL(file);
  };
  return (
    <Stack>
      <Stack hidden={nextStep} spacing={1}>
        <CustomizedField
          type="text"
          placeholder="Nhập mã giảm giá"
          startAdornment={''}
          name="enterVoucher"
          value={voucherCode}
          onChange={(e) => setVoucherCode(e.target.value)}
        />
        <Stack className={cx('row_camera_scan')}>
          <Typography className={cx('text_grey_choices')}>Hoặc</Typography>
          <WhiteButton
            variant="file"
            startIcon={<CameraIcon />}
            component="label"
          >
            Quét mã KM
            <input
              hidden
              accept="image/*"
              multiple
              type="file"
              onChange={(e) => handleFileChange(e)}
            />
          </WhiteButton>
        </Stack>
        <Typography>
          Mã hợp lệ<br></br>Trị giá voucher: 50.000 đ
        </Typography>
        <MmenuButton onClick={() => setNextStep(!nextStep)}>
          Tiếp theo
        </MmenuButton>
      </Stack>
      <CreateOrderInformation hidden={!nextStep}></CreateOrderInformation>
    </Stack>
  );
}
