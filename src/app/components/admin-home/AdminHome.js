import { IconButton, Stack, Typography } from '@mui/material';
import Image from 'next/image';
import {
  ShoppingCart,
  PeopleAlt,
  SupportAgent,
  Discount,
} from '@mui/icons-material';
import { useContext } from 'react';
import { MallContext } from '@/app/state-manager/mallContext';
import { DirectContant } from '@/app/utils/contants';

const AdminHome = () => {
  const { dispatch } = useContext(MallContext);
  return (
    <Stack alignItems={'center'} paddingTop={5}>
      <Image src={'/iph-logo.svg'} width={200} height={58} alt=""></Image>
      <Stack
        paddingTop={20}
        width={1}
        direction={'row'}
        justifyContent={'space-around'}
      >
        <Stack color={'black'} alignItems={'center'} spacing={1}>
          <IconButton
            size="large"
            sx={{
              padding: '1rem',
              bgcolor: 'black !important',
              color: 'white',
              width: '120px',
              height: '120px',
              borderRadius: '8px',
            }}
            onClick={() =>
              dispatch({
                type: DirectContant.DIRECT_TO_ADMIN_STORE,
              })
            }
          >
            <ShoppingCart sx={{ fontSize: '50px' }}></ShoppingCart>
          </IconButton>
          <Typography fontWeight={800}>QL gian hàng</Typography>
        </Stack>
        <Stack color={'black'} alignItems={'center'} spacing={1}>
          <IconButton
            size="large"
            sx={{
              padding: '1rem',
              bgcolor: 'black !important',
              color: 'white',
              width: '120px',
              height: '120px',
              borderRadius: '8px',
            }}
          >
            <PeopleAlt sx={{ fontSize: '50px' }}></PeopleAlt>
          </IconButton>
          <Typography fontWeight={800}>QL Nhân viên</Typography>
        </Stack>
        <Stack color={'black'} alignItems={'center'} spacing={1}>
          <IconButton
            size="large"
            sx={{
              padding: '1rem',
              bgcolor: 'black !important',
              color: 'white',
              width: '120px',
              height: '120px',
              borderRadius: '8px',
            }}
            onClick={() =>
              dispatch({
                type: DirectContant.DIRECT_TO_ADMIN_CUSTOMER,
              })
            }
          >
            <SupportAgent sx={{ fontSize: '50px' }}></SupportAgent>
          </IconButton>
          <Typography fontWeight={800}>QL Khách hàng</Typography>
        </Stack>
        <Stack color={'black'} alignItems={'center'} spacing={1}>
          <IconButton
            size="large"
            sx={{
              padding: '1rem',
              bgcolor: 'black !important',
              color: 'white',
              width: '120px',
              height: '120px',
              borderRadius: '8px',
            }}
            onClick={() =>
              dispatch({
                type: DirectContant.DIRECT_TO_ADMIN_PROMOTION,
              })
            }
          >
            <Discount sx={{ fontSize: '50px' }}></Discount>
          </IconButton>
          <Typography fontWeight={800}>QL Khuyến mãi</Typography>
        </Stack>
      </Stack>
    </Stack>
  );
};

export default AdminHome;
