import classNames from 'classnames/bind';
import {
  Button,
  Stack,
  ThemeProvider,
  Typography,
  createTheme,
} from '@mui/material';
import styles from './MmenuButton.module.css';

const cx = classNames.bind(styles);

const theme = createTheme({
  typography: {
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
  },
  palette: {
    primary: {
      main: '#fff',
    },
  },
});

const MmenuWhiteButton = ({ title, description, onClick, props, children }) => {
  return (
    <ThemeProvider theme={theme}>
      <Stack position={'relative'} justifyContent={'center'} width={1}>
        <Button
          color="primary"
          variant="contained"
          className={cx('mmenu__button', 'mmenu__white-button')}
          onClick={onClick}
          {...props}
        >
          <Stack spacing={1} direction={'row'}>
            <Typography fontSize={16} fontWeight={600}>
              {title || 'Xác nhận'}
            </Typography>
            <Typography fontSize={16} fontWeight={600} color={'#AFAFAF'}>
              {description}
            </Typography>
          </Stack>
        </Button>
        <Stack width={24} height={24} position={'absolute'} right={12}>
          {children}
        </Stack>
      </Stack>
    </ThemeProvider>
  );
};

export default MmenuWhiteButton;
