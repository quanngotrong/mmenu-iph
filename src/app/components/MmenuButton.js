import { Button } from '@mui/material';
import { styled } from '@mui/material/styles';

const MmenuButton = styled(Button)({
  textTransform: 'none',
  width: '100%',
  fontSize: 18,
  fontWeight: 600,
  padding: '0.8rem',
  backgroundColor: 'black !important',
  color: 'white',
  borderRadius: '8px',
});

export default MmenuButton;
