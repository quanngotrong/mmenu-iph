/* eslint-disable import/no-extraneous-dependencies */
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import Toolbar from '@mui/material/Toolbar';
import * as React from 'react';
import Image from 'next/image';
import classNames from 'classnames/bind';
import { useBackButton } from '@/app/utils/utils';
import styles from './AppBar.module.css';

const cx = classNames.bind(styles);

export default function Appbar() {
  const handleBack = useBackButton();
  return (
    <Toolbar className={cx('toolbar')}>
      <IconButton
        edge="start"
        aria-label="menu"
        sx={{ color: '#000' }}
        onClick={handleBack}
      >
        <ArrowBackIosIcon />
      </IconButton>
      <Image src={'/iph-logo.svg'} width={110} height={32} alt=""></Image>
      <IconButton edge="start" aria-label="menu" sx={{ color: '#000' }}>
        <MenuIcon />
      </IconButton>
    </Toolbar>
  );
}
