import {
  Dialog,
  DialogTitle,
  Stack,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
} from '@mui/material';
import React from 'react';
import CustomizedField from '../CustomerField';
import MmenuButton from '../MmenuButton';
import MmenuWhiteButton from '../MmenuWhiteButton';

const DialogCreateEmployee = ({ open, handleCreateStore, handleOnClose }) => {
  const [area, setArea] = React.useState('');

  const handleChange = (event) => {
    setArea(event.target.value);
  };
  return (
    <Dialog
      open={open}
      onClose={handleOnClose}
      maxWidth={'xs'}
      fullWidth
      PaperProps={{
        maxHeight: '80vh',
      }}
    >
      <DialogTitle textAlign={'center'} fontSize={20} fontWeight={700}>
        Thêm mới gian hàng
      </DialogTitle>
      <Stack alignItems={'center'} p={2} spacing={2}>
        <CustomizedField
          label="Họ tên"
          type="text"
          sx={{ mb: 0 }}
        ></CustomizedField>
        <CustomizedField label="Số điện thoại" type="text"></CustomizedField>
        <CustomizedField label="Mật khẩu" type="text"></CustomizedField>
        <CustomizedField
          label="Xác nhận lại mật khẩu"
          type="text"
        ></CustomizedField>
        <FormControl fullWidth>
          <InputLabel id="demo-simple-select-label">Chọn bộ phận</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={area}
            label="Age"
            onChange={handleChange}
          >
            <MenuItem value={10}>Ten</MenuItem>
            <MenuItem value={20}>Twenty</MenuItem>
            <MenuItem value={30}>Thirty</MenuItem>
          </Select>
        </FormControl>
        <FormControl fullWidth>
          <InputLabel id="demo-simple-select-label">Chọn gian hàng</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={area}
            label="Age"
            onChange={handleChange}
          >
            <MenuItem value={10}>Ten</MenuItem>
            <MenuItem value={20}>Twenty</MenuItem>
            <MenuItem value={30}>Thirty</MenuItem>
          </Select>
        </FormControl>
        <Stack direction={'row'} width={1} spacing={2}>
          <MmenuWhiteButton
            title={'Đóng'}
            onClick={handleOnClose}
          ></MmenuWhiteButton>
          <MmenuButton onClick={handleCreateStore}>Thêm mới</MmenuButton>
        </Stack>
      </Stack>
    </Dialog>
  );
};

export default DialogCreateEmployee;
