import {
  Stack,
  List,
  ListItemButton,
  ListItem,
  ListItemText,
  Collapse,
  IconButton,
} from '@mui/material';
import React, { useState } from 'react';
import {
  ControlPoint,
  ExpandLess,
  ExpandMore,
  BorderColor,
} from '@mui/icons-material';
import MmenuButton from '../MmenuButton';
import DialogCreateEmployee from './DialogCreateEmployee';

const EmployeeTab = () => {
  const [open, setOpen] = React.useState(true);

  const handleClick = () => {
    setOpen(!open);
  };
  const [area, setArea] = React.useState('');
  const [openDialogCreateEmployee, setOpenDialogCreateEmployee] =
    useState(false);

  const handleChange = (event) => {
    setArea(event.target.value);
  };
  return (
    <Stack spacing={2}>
      <Stack p={'24px 24px 0'}>
        <MmenuButton
          onClick={() => setOpenDialogCreateEmployee(true)}
          startIcon={<ControlPoint />}
        >
          Thêm mới nhân viên
        </MmenuButton>
      </Stack>
      <Stack spacing={1}>
        <ListItemButton
          onClick={handleClick}
          sx={{ p: 2, border: '1px solid #E2E2E2' }}
        >
          <ListItemText primary="GIAN HÀNG 1" />
          {open ? <ExpandLess /> : <ExpandMore />}
        </ListItemButton>
        <Collapse in={open} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            <ListItem
              sx={{ pl: 4, border: '1px solid #E2E2E2', p: 2 }}
              secondaryAction={
                <IconButton edge="end" aria-label="delete">
                  <BorderColor />
                </IconButton>
              }
            >
              <ListItemText primary="Nhân viên 1" />
            </ListItem>
            <ListItem
              sx={{ pl: 4, border: '1px solid #E2E2E2', p: 2 }}
              secondaryAction={
                <IconButton edge="end" aria-label="delete">
                  <BorderColor />
                </IconButton>
              }
            >
              <ListItemText primary="Nhân viên 1" />
            </ListItem>
            <ListItem
              sx={{ pl: 4, border: '1px solid #E2E2E2', p: 2 }}
              secondaryAction={
                <IconButton edge="end" aria-label="delete">
                  <BorderColor />
                </IconButton>
              }
            >
              <ListItemText primary="Nhân viên 1" />
            </ListItem>
          </List>
        </Collapse>
      </Stack>
      <DialogCreateEmployee
        open={openDialogCreateEmployee}
        handleOnClose={() => setOpenDialogCreateEmployee(false)}
      ></DialogCreateEmployee>
    </Stack>
  );
};

export default EmployeeTab;
