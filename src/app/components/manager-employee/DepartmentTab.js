import { Stack } from '@mui/material';
import React, { useState } from 'react';
import { ControlPoint, BorderColor } from '@mui/icons-material';
import MmenuButton from '../MmenuButton';
import MmenuWhiteButton from '../MmenuWhiteButton';
// import DialogCreateArea from './DialogCreateArea';

const DepartmentTab = () => {
  const [openDialogCreateArea, setOpenDialogCreateArea] = useState(false);
  return (
    <Stack spacing={2}>
      <MmenuButton
        fullWidth
        onClick={() => setOpenDialogCreateArea(true)}
        startIcon={<ControlPoint />}
      >
        Thêm mới bộ phận
      </MmenuButton>
      <Stack spacing={1}>
        <MmenuWhiteButton
          title={'QUẢN LÝ TTTM'}
          description={'TTTM'}
          props={{
            sx: { justifyContent: 'start' },
          }}
        >
          <BorderColor />
        </MmenuWhiteButton>
        <MmenuWhiteButton
          title={'QUẢN LÝ TTTM'}
          description={'TTTM'}
          props={{
            sx: { justifyContent: 'start' },
          }}
        >
          <BorderColor />
        </MmenuWhiteButton>
        <MmenuWhiteButton
          title={'QUẢN LÝ TTTM'}
          description={'TTTM'}
          props={{
            sx: { justifyContent: 'start' },
          }}
        >
          <BorderColor />
        </MmenuWhiteButton>
      </Stack>
      {/* <DialogCreateArea
        open={openDialogCreateArea}
        handleOnClose={() => setOpenDialogCreateArea(false)}
      ></DialogCreateArea> */}
    </Stack>
  );
};

export default DepartmentTab;
