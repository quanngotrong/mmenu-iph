import {
  Stack,
  AppBar,
  Toolbar,
  Typography,
  Box,
  Tabs,
  Tab,
} from '@mui/material';
import React from 'react';
import { ArrowBackIosRounded } from '@mui/icons-material';
import Promotion from './Promotion';
import CheckCode from './CheckCode';
import { useBackButton } from '@/app/utils/utils';
import Report from './Report';

function a11yProps(index) {
  return {
    id: `vertical-tab-${index}`,
    'aria-controls': `vertical-tabpanel-${index}`,
  };
}

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <Stack
      role="tabpanel"
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...other}
      m={2}
      width={'-webkit-fill-available'}
    >
      {value === index && <Box>{children}</Box>}
    </Stack>
  );
}

const ManagerPromotion = () => {
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  const handleBack = useBackButton();
  return (
    <Stack bgcolor={'white'} color={'black'}>
      <AppBar
        position="fixed"
        sx={{
          zIndex: (theme) => theme.zIndex.drawer + 1,
          backgroundColor: 'white',
          boxShadow: 'none',
          borderBottom: '1px solid #E2E2E2',
          color: 'black',
        }}
      >
        <Toolbar sx={{ justifyContent: 'space-between' }}>
          <ArrowBackIosRounded onClick={handleBack} />
          <Typography
            variant="h6"
            noWrap
            component="div"
            textTransform={'uppercase'}
            fontSize={16}
          >
            Quản lý KHUYẾN MẠI
          </Typography>
          <Typography></Typography>
        </Toolbar>
      </AppBar>
      <Box sx={{ display: 'flex' }} minHeight={'100vh'} mt={'60px'}>
        <Tabs
          orientation="vertical"
          variant="scrollable"
          value={value}
          onChange={handleChange}
          aria-label="Vertical tabs example"
          sx={{
            borderRight: 1,
            borderColor: 'divider',
            minWidth: '200px',
            '& .Mui-selected': {
              bgcolor: 'black',
              color: 'white !important',
              border: 'none',
            },
            '& .MuiTab-root': {
              height: 60,
              width: '100%',
            },
            '& .MuiTabs-indicator': { display: 'none' },
          }}
        >
          <Tab label="CTKM" {...a11yProps(0)} />
          <Tab label="Duyệt mã" {...a11yProps(1)} />
          <Tab label="Báo cáo" {...a11yProps(2)} />
        </Tabs>
        <TabPanel value={value} index={0}>
          <Promotion></Promotion>
        </TabPanel>
        <TabPanel value={value} index={1}>
          <CheckCode></CheckCode>
        </TabPanel>
        <TabPanel value={value} index={2}>
          <Report></Report>
        </TabPanel>
      </Box>
    </Stack>
  );
};

export default ManagerPromotion;
