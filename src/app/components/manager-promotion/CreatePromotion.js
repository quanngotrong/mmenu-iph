import { Button, Stack, Typography } from '@mui/material';
import { LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { MobileDateRangePicker } from '@mui/x-date-pickers-pro';
import Radio from '@mui/joy/Radio';
import { RadioGroup } from '@mui/joy';
import { FileUpload } from '@mui/icons-material';
import { useState } from 'react';
import Link from 'next/link';
import CustomizedField from '../CustomerField';
import MmenuButton from '../MmenuButton';

const CreatePromotion = ({ promotionSelector, onClose, open }) => {
  const { isUpdate } = promotionSelector;
  const [checkboxValue, setCheckboxValue] = useState('');
  return (
    <Stack spacing={2} width={0.8} pb={12} hidden={!open}>
      <Typography variant="h1" fontSize={16} fontWeight={600} mb={1}>
        Tên CTKM
      </Typography>
      <CustomizedField placeholder="Nhập tên CTKM" required></CustomizedField>
      <Typography variant="h1" fontSize={16} fontWeight={600} mb={1}>
        Mô tả
      </Typography>
      <CustomizedField placeholder="Nhập mô tả" required></CustomizedField>
      <Typography variant="h1" fontSize={16} fontWeight={600} mb={1}>
        Thời gian triển khai
      </Typography>
      <LocalizationProvider dateAdapter={AdapterDayjs}>
        <MobileDateRangePicker
          localeText={{ start: 'Ngày bắt đầu', end: 'Ngày kết thúc' }}
          sx={{ mb: 2 }}
        />
      </LocalizationProvider>
      <Typography variant="h1" fontSize={16} fontWeight={600} mb={1}>
        Giá trị Voucher
      </Typography>
      <CustomizedField
        placeholder="Nhập giá trị Voucher"
        required
      ></CustomizedField>
      {isUpdate ? (
        <>
          <Typography variant="h1" fontSize={16} fontWeight={600} mb={1}>
            Số lượng Voucher phát hành
          </Typography>
          <CustomizedField placeholder="1.000" required></CustomizedField>
          <Typography variant="h1" fontSize={16} fontWeight={600} mb={1}>
            Danh sách mã
          </Typography>
          <Stack direction={'row'} spacing={0.5}>
            <Typography variant="h1" fontSize={16} mb={1}>
              Tải danh sách
            </Typography>
            <Link href={'#'}>
              <Typography variant="h1" fontSize={16} fontWeight={600} mb={1}>
                tại đây
              </Typography>
            </Link>
          </Stack>
        </>
      ) : (
        <>
          <Typography variant="h1" fontSize={16} fontWeight={600} mb={1}>
            Thiết lập mã CTKM
          </Typography>
          <Typography variant="h1" fontSize={16} fontWeight={600} mb={1}>
            Hình thức tạo mã
          </Typography>
          <RadioGroup name="radio-buttons-group" orientation="horizontal">
            <Radio
              color="primary"
              orientation="horizontal"
              size="lg"
              variant="outlined"
              value={'thu-cong'}
              label="Thủ công"
              onChange={() => setCheckboxValue('thu-cong')}
            />
            <Radio
              color="primary"
              orientation="horizontal"
              size="lg"
              variant="outlined"
              value={'tu-dong'}
              label="Tự động"
              onChange={() => setCheckboxValue('tu-dong')}
            />
          </RadioGroup>
          {checkboxValue === 'thu-cong' && (
            <>
              <Stack direction={'row'} alignItems={'center'} spacing={2}>
                <Stack
                  direction={'row'}
                  alignItems={'center'}
                  spacing={2}
                  p={2}
                  border={'1px solid #E2E2E2'}
                  width={'fit-content'}
                  borderRadius={'8px'}
                >
                  <Button
                    component="label"
                    endIcon={<FileUpload></FileUpload>}
                    sx={{
                      bgcolor: 'black',
                      color: 'white',
                      p: 2,
                      textTransform: 'capitalize',
                      borderRadius: '8px',
                    }}
                  >
                    <Typography variant="h1" fontSize={16} fontWeight={700}>
                      Nhập file
                    </Typography>
                    <input hidden accept="image/*" multiple type="file" />
                  </Button>
                  <Typography color={'#AFAFAF'}>Chọn file để nhập</Typography>
                </Stack>
                <Stack direction={'row'} spacing={1}>
                  <Typography variant="h1" fontSize={16}>
                    Tải file mẫu{' '}
                  </Typography>
                  <Typography variant="h1" fontSize={16} fontWeight={700}>
                    tại đây
                  </Typography>
                </Stack>
              </Stack>
              <CustomizedField
                label={'Nhập mã, mỗi mã cách nhau bởi dấu “,”'}
                multiline
                rows={4}
              ></CustomizedField>
            </>
          )}
          {checkboxValue === 'tu-dong' && (
            <Stack
              direction={'row'}
              spacing={2}
              position={'relative'}
              alignItems={'flex-end'}
            >
              <Stack width={0.5}>
                <Typography variant="h1" fontSize={16} fontWeight={600} mb={1}>
                  Số lượng mã cần tạo
                </Typography>
                <CustomizedField
                  placeholder="Nhập số"
                  required
                ></CustomizedField>
              </Stack>
              <Stack width={0.5}>
                <Typography variant="h1" fontSize={16} fontWeight={600} mb={1}>
                  Số kí tự của mã
                </Typography>
                <CustomizedField
                  placeholder="Nhập số"
                  required
                ></CustomizedField>
              </Stack>
              <Stack width={0.2} position={'absolute'} right={'-23%'}>
                <MmenuButton sx={{ mb: '16px' }}>Chọn lại</MmenuButton>
              </Stack>
            </Stack>
          )}
        </>
      )}
      <Stack
        direction={'row'}
        boxShadow={'0px -2px 6px rgba(0, 0, 0, 0.1)'}
        p={2}
        position={'fixed'}
        width={'calc(100% + 32px)'}
        bgcolor={'white'}
        bottom={0}
        left={'-16px'}
        spacing={2}
        justifyContent={'flex-end'}
        pr={4}
        zIndex={10}
      >
        <MmenuButton
          sx={{
            width: '15%',
            bgcolor: 'white !important',
            color: 'black',
            border: '1px solid #E2E2E2',
          }}
          onClick={onClose}
        >
          Hủy
        </MmenuButton>
        <MmenuButton sx={{ width: '15%' }}>Xác nhận</MmenuButton>
      </Stack>
    </Stack>
  );
};

export default CreatePromotion;
