import {
  Stack,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  InputAdornment,
  TableContainer,
  Table,
  TableHead,
  TableCell,
  TableBody,
  Pagination,
  TableRow,
} from '@mui/material';
import React, { useState } from 'react';
import { LocalizationProvider, DateField } from '@mui/x-date-pickers';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { Search, Download } from '@mui/icons-material';
import styled from '@emotion/styled';
import CustomizedField from '../CustomerField';
import MmenuButton from '../MmenuButton';
import CreatePromotion from './CreatePromotion';

const columns = [
  { id: 'serial', label: 'STT', minWidth: 50 },
  { id: 'id', label: 'Thời gian', minWidth: 120 },
  {
    id: 'number',
    label: 'Hóa đơn',
    minWidth: 170,
    align: 'right',
    format: (value) => value.toLocaleString('en-US'),
  },
  {
    id: 'time',
    label: 'Tổng số tiền',
    minWidth: 170,
    align: 'right',
    format: (value) => value.toLocaleString('en-US'),
  },
  {
    id: 'total',
    label: 'Mã Voucher',
    minWidth: 170,
    align: 'right',
    format: (value) => value.toFixed(2),
  },
  {
    id: 'dícount',
    label: 'Số tiền giảm giá',
    minWidth: 170,
    align: 'right',
    format: (value) => value.toFixed(2),
  },
  {
    id: 'dícount',
    label: 'Cửa hàng',
    minWidth: 170,
    align: 'right',
    format: (value) => value.toFixed(2),
  },
];

function createData(serial, id, number, time) {
  const density = number / time;
  return { serial, id, number, time, density };
}

const StyledTableRow = styled(TableRow)(() => ({
  // hide last border
  '&:last-child td, &:last-child th': {
    border: 2,
  },
}));

const rows = [
  createData('India', 'IN', 1324171354, 3287263),
  createData('China', 'CN', 1403500365, 9596961),
  createData('Italy', 'IT', 60483973, 301340),
  createData('United States', 'US', 327167434, 9833520),
  createData('Canada', 'CA', 37602103, 9984670),
  createData('Australia', 'AU', 25475400, 7692024),
  createData('Germany', 'DE', 83019200, 357578),
  createData('Ireland', 'IE', 4857000, 70273),
  createData('Mexico', 'MX', 126577691, 1972550),
  createData('Japan', 'JP', 126317000, 377973),
  createData('France', 'FR', 67022000, 640679),
  createData('United Kingdom', 'GB', 67545757, 242495),
  createData('Russia', 'RU', 146793744, 17098246),
  createData('Nigeria', 'NG', 200962417, 923768),
  createData('Brazil', 'BR', 210147125, 8515767),
];

const Report = () => {
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [createPromotion, setCreatePromotion] = React.useState(false);
  const [promotionSelector, setPromotionSelector] = useState({});

  const handleOpenCreatePromotion = () => {
    setCreatePromotion(true);
  };

  const handleOpenUpdate = (promotion) => {
    setPromotionSelector(promotion);
    setCreatePromotion(true);
  };

  const handleOnclose = () => {
    setCreatePromotion(false);
    setPromotionSelector({});
  };

  return (
    <Stack>
      <Stack hidden={createPromotion}>
        <Stack direction={'row'} spacing={2} alignItems={'center'} mt={1}>
          <LocalizationProvider dateAdapter={AdapterDayjs}>
            <DateField label="Thời gian" sx={{ width: '30%' }}></DateField>
          </LocalizationProvider>
          <FormControl
            fullWidth
            sx={{
              margin: '8px 0 0 12px',
              width: '30%',
            }}
          >
            <InputLabel id="demo-simple-select-label">
              Chọn gian hàng
            </InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              label="Age"
            >
              <MenuItem value={'male'}>Nam</MenuItem>
              <MenuItem value={'female'}>Nữ</MenuItem>
              <MenuItem value={'orther'}>Khác</MenuItem>
            </Select>
          </FormControl>
          <CustomizedField
            placeholder={'Search'}
            sx={{
              width: '30%',
            }}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <Search></Search>
                </InputAdornment>
              ),
            }}
          ></CustomizedField>
          <MmenuButton
            sx={{ width: '30%' }}
            endIcon={<Download></Download>}
            onClick={handleOpenCreatePromotion}
          >
            Export
          </MmenuButton>
        </Stack>
        <TableContainer sx={{ maxHeight: 440, mt: 2 }}>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <StyledTableRow>
                {columns.map((column) => (
                  <TableCell
                    key={column.id}
                    align={column.align}
                    style={{ minWidth: column.minWidth }}
                  >
                    {column.label}
                  </TableCell>
                ))}
              </StyledTableRow>
            </TableHead>
            <TableBody>
              {rows
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row) => {
                  return (
                    <StyledTableRow
                      hover
                      role="checkbox"
                      tabIndex={-1}
                      key={row.code}
                      sx={{
                        border: 1,
                      }}
                      onClick={() => handleOpenUpdate({ isUpdate: true })}
                    >
                      {columns.map((column) => {
                        const value = row[column.id];
                        return (
                          <TableCell key={column.id} align={column.align}>
                            {column.format && typeof value === 'number'
                              ? column.format(value)
                              : value}
                          </TableCell>
                        );
                      })}
                    </StyledTableRow>
                  );
                })}
            </TableBody>
          </Table>
        </TableContainer>
        <Stack alignItems={'center'} mt={'50px'}>
          <Pagination count={10}></Pagination>
        </Stack>
      </Stack>
      <CreatePromotion
        open={createPromotion}
        onClose={() => handleOnclose()}
        promotionSelector={promotionSelector}
      ></CreatePromotion>
    </Stack>
  );
};

export default Report;
