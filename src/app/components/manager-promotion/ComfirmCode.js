import {
  Stack,
  Typography,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Card,
} from '@mui/material';
import React, { useState } from 'react';
import { LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { MobileDateRangePicker } from '@mui/x-date-pickers-pro';
import Image from 'next/image';
import CustomizedField from '../CustomerField';
import MmenuButton from '../MmenuButton';

const ComfirmCode = ({onClose}) => {
  const [invoiceCode, setInvoiceCode] = React.useState('');
  const handlStatusChange = (event) => {
    setInvoiceCode(event.target.value);
  };
  const [currentImage, setCurrentImage] = useState(
    '/background-images-large.svg'
  );

  const handleChangeImage = ({ key, value }) => {
    setCurrentImage(value);
    console.log(value);
  };
  return (
    <Stack direction={'row'} spacing={2} pb={12}>
      <Stack spacing={2} width={0.6}>
        <Typography>Gian hàng</Typography>
        <CustomizedField placeholder="Gian hàng"></CustomizedField>
        <Typography>Thời gian yêu cầu</Typography>
        <LocalizationProvider dateAdapter={AdapterDayjs}>
          <MobileDateRangePicker
            localeText={{ start: 'Ngày bắt đầu', end: 'Ngày kết thúc' }}
            sx={{ mb: 2 }}
          />
        </LocalizationProvider>
        <Typography>Tên khách hàng</Typography>
        <CustomizedField placeholder="Tên khách hàng"></CustomizedField>
        <Typography>Số điện thoại khách hàng</Typography>
        <CustomizedField placeholder="Số điện thoại khách hàng"></CustomizedField>
        <Typography>Số tiền trên hóa đơn</Typography>
        <CustomizedField placeholder="Số tiền trên hóa đơn"></CustomizedField>
        <Typography>Số hóa đơn</Typography>
        <FormControl fullWidth>
          <InputLabel id="demo-simple-select-label">Số hóa đơn</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={invoiceCode}
            label="Age"
            onChange={handlStatusChange}
          >
            <MenuItem value={10}>Phúc Long Coffee</MenuItem>
            <MenuItem value={20}>Twenty</MenuItem>
            <MenuItem value={30}>Thirty</MenuItem>
          </Select>
        </FormControl>
      </Stack>
      <Stack width={0.4}>
        <Typography variant="h1" fontSize={16} fontWeight={700}>
          Ảnh hóa đơn
        </Typography>
        <Stack alignItems={'center'} p={'64px 0 32px'}>
          <Card sx={{ width: 300, height: 300 }}>
            <Image src={currentImage} width={300} height={300} alt=""></Image>
          </Card>
        </Stack>
        <Stack direction={'row'} justifyContent={'center'} spacing={2}>
          <Card
            sx={{
              width: 60,
              height: 60,
            }}
            onClick={() =>
              handleChangeImage({ value: '/background-images.svg' })
            }
          >
            <Image
              src={'/background-images.svg'}
              width={60}
              height={60}
              alt=""
            ></Image>
          </Card>
          <Card
            sx={{
              width: 60,
              height: 60,
            }}
            onClick={() =>
              handleChangeImage({ value: '/background-images-large.svg' })
            }
          >
            <Image
              src={'/background-images-large.svg'}
              width={60}
              height={60}
              alt=""
            ></Image>
          </Card>
          <Card
            sx={{
              width: 60,
              height: 60,
            }}
            onClick={() =>
              handleChangeImage({ value: '/background-images.svg' })
            }
          >
            <Image
              src={'/background-images.svg'}
              width={60}
              height={60}
              alt=""
            ></Image>
          </Card>
        </Stack>
      </Stack>
      <Stack
        direction={'row'}
        boxShadow={'0px -2px 6px rgba(0, 0, 0, 0.1)'}
        p={2}
        position={'fixed'}
        width={1}
        bgcolor={'white'}
        bottom={0}
        left={'-16px'}
        spacing={2}
        justifyContent={'flex-end'}
        pr={4}
        zIndex={10}
      >
        <MmenuButton
          sx={{
            width: '15%',
            bgcolor: 'white !important',
            color: 'black',
            border: '1px solid #E2E2E2',
          }}
          onClick={onClose}
        >
          Hủy
        </MmenuButton>
        <MmenuButton sx={{ width: '15%' }}>Xác nhận</MmenuButton>
      </Stack>
    </Stack>
  );
};

export default ComfirmCode;
