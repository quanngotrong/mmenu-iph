import {
  Stack,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  InputAdornment,
  TableContainer,
  TableHead,
  Table,
  TableRow,
  TableCell,
  TableBody,
  Pagination,
} from '@mui/material';
import React from 'react';
import { LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { MobileDateRangePicker } from '@mui/x-date-pickers-pro';
import { Search } from '@mui/icons-material';
import styled from '@emotion/styled';
import CustomizedField from '../CustomerField';
import ComfirmCode from './ComfirmCode';

const columns = [
  { id: 'serial', label: 'STT', minWidth: 50 },
  { id: 'id', label: 'Thời gian', minWidth: 120 },
  {
    id: 'number',
    label: 'Gian hàng',
    minWidth: 170,
    align: 'right',
    format: (value) => value.toLocaleString('en-US'),
  },
  {
    id: 'time',
    label: 'Số hóa đơn',
    minWidth: 170,
    align: 'right',
    format: (value) => value.toLocaleString('en-US'),
  },
  {
    id: 'total',
    label: 'Giá trị hóa đơn',
    minWidth: 170,
    align: 'right',
    format: (value) => value.toFixed(2),
  },
  {
    id: 'dícount',
    label: 'Giá trị Voucher',
    minWidth: 170,
    align: 'right',
    format: (value) => value.toFixed(2),
  },
  {
    id: 'dícount',
    label: 'Trạng thái',
    minWidth: 170,
    align: 'right',
    format: (value) => value.toFixed(2),
  },
];

function createData(serial, id, number, time) {
  const density = number / time;
  return { serial, id, number, time, density };
}

const StyledTableRow = styled(TableRow)(() => ({
  // hide last border
  '&:last-child td, &:last-child th': {
    border: 2,
  },
}));

const rows = [
  createData('India', 'IN', 1324171354, 3287263),
  createData('China', 'CN', 1403500365, 9596961),
  createData('Italy', 'IT', 60483973, 301340),
  createData('United States', 'US', 327167434, 9833520),
  createData('Canada', 'CA', 37602103, 9984670),
  createData('Australia', 'AU', 25475400, 7692024),
  createData('Germany', 'DE', 83019200, 357578),
  createData('Ireland', 'IE', 4857000, 70273),
  createData('Mexico', 'MX', 126577691, 1972550),
  createData('Japan', 'JP', 126317000, 377973),
  createData('France', 'FR', 67022000, 640679),
  createData('United Kingdom', 'GB', 67545757, 242495),
  createData('Russia', 'RU', 146793744, 17098246),
  createData('Nigeria', 'NG', 200962417, 923768),
  createData('Brazil', 'BR', 210147125, 8515767),
];

const CheckCode = () => {
  const [store, setStore] = React.useState('');
  const [status, setStatus] = React.useState('');
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [comfirmCode, setComfirmCode] = React.useState(false);

  const handleStoreChange = (event) => {
    setStore(event.target.value);
  };
  const handlStatusChange = (event) => {
    setStatus(event.target.value);
  };

  const handleOpenComfirmCode = () => {
    setComfirmCode(true);
  }

  return (
    <Stack>
      {!comfirmCode ? (
        <>
          <Stack direction={'row'} spacing={2}>
            <FormControl fullWidth>
              <InputLabel id="demo-simple-select-label">
                Chọn gian hàng
              </InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={store}
                label="Age"
                onChange={handleStoreChange}
              >
                <MenuItem value={10}>Phúc Long Coffee</MenuItem>
                <MenuItem value={20}>Twenty</MenuItem>
                <MenuItem value={30}>Thirty</MenuItem>
              </Select>
            </FormControl>
            <FormControl fullWidth>
              <InputLabel id="demo-simple-select-label">Trạng thái</InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={status}
                label="Age"
                onChange={handlStatusChange}
              >
                <MenuItem value={10}>Phúc Long Coffee</MenuItem>
                <MenuItem value={20}>Twenty</MenuItem>
                <MenuItem value={30}>Thirty</MenuItem>
              </Select>
            </FormControl>
            <Stack width={1}>
              <LocalizationProvider dateAdapter={AdapterDayjs}>
                <MobileDateRangePicker
                  localeText={{ start: 'Ngày bắt đầu', end: 'Ngày kết thúc' }}
                  sx={{ mb: 2, width: 1 }}
                />
              </LocalizationProvider>
            </Stack>
            <CustomizedField
              placeholder={'Search'}
              sx={{
                width: 1,
              }}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <Search></Search>
                  </InputAdornment>
                ),
              }}
            ></CustomizedField>
          </Stack>
          <TableContainer sx={{ maxHeight: 440, mt: 2 }}>
            <Table stickyHeader aria-label="sticky table">
              <TableHead>
                <StyledTableRow>
                  {columns.map((column) => (
                    <TableCell
                      key={column.id}
                      align={column.align}
                      style={{ minWidth: column.minWidth }}
                    >
                      {column.label}
                    </TableCell>
                  ))}
                </StyledTableRow>
              </TableHead>
              <TableBody>
                {rows
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row) => {
                    return (
                      <StyledTableRow
                        hover
                        role="checkbox"
                        tabIndex={-1}
                        key={row.code}
                        sx={{
                          border: 1,
                        }}
                        onClick={() => handleOpenComfirmCode()}
                      >
                        {columns.map((column) => {
                          const value = row[column.id];
                          return (
                            <TableCell key={column.id} align={column.align}>
                              {column.format && typeof value === 'number'
                                ? column.format(value)
                                : value}
                            </TableCell>
                          );
                        })}
                      </StyledTableRow>
                    );
                  })}
              </TableBody>
            </Table>
          </TableContainer>
          <Stack alignItems={'center'} mt={'50px'}>
            <Pagination count={10}></Pagination>
          </Stack>
        </>
      ) : (
        <ComfirmCode onClose={() => setComfirmCode(false)}></ComfirmCode>
      )}
    </Stack>
  );
};

export default CheckCode;
