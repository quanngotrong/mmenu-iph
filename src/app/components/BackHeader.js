import { Stack, Typography } from '@mui/material';
import { ArrowBackIosRounded } from '@mui/icons-material';
import { useBackButton } from '../utils/utils';

const BackHeader = ({ displayText, children, props }) => {
  const handleBack = useBackButton();
  return (
    <Stack
      direction={'row'}
      p={2}
      width={1}
      alignItems={'center'}
      justifyContent={'space-between'}
      bgcolor={'white'}
      {...props}
    >
      <ArrowBackIosRounded onClick={handleBack} />
      <Typography variant="subtitle1" textTransform={'uppercase'}>
        {displayText}
      </Typography>
      <Stack>{children}</Stack>
    </Stack>
  );
};

export default BackHeader;
