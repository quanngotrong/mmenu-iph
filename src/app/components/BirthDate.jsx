import React from 'react';
import { TextField, InputAdornment, Typography } from '@mui/material';

const inputStyle = {
  border: 'none',
  backgroundColor: '#eeeeee',
  height: '3rem',
  width: 1,
  fontSize: '0.75rem',
  borderRadius: '8px',
};

const BirthDateInput = () => {
  return (
    <TextField
      label="Ngày sinh"
      type="text"
      InputProps={{
        endAdornment: (
          <InputAdornment position="end">
            <Typography variant="body2" color="textSecondary">
              (dd/mm/yy)
            </Typography>
          </InputAdornment>
        ),
        sx: inputStyle,
      }}
      sx={{ '& label': { fontSize: '0.8rem' } }}
    />
  );
};

export default BirthDateInput;
