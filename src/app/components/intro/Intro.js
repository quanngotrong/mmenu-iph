import classNames from 'classnames/bind';
import Image from 'next/image';
import { Stack, Typography } from '@mui/material';
import styles from './Intro.module.css';
import MmenuButton from '../MmenuButton';

const cx = classNames.bind(styles);

const IntroComponents = () => {
  return (
    <Stack
      className={cx('home')}
      alignItems={'center'}
      justifyContent={'space-between'}
      height={'100vh'}
      spacing={2}
      paddingTop={10}
    >
      <Image src={'/iph-logo.svg'} width={110} height={32} alt=""></Image>
      <Stack className={cx('home__bottom')} spacing={2}>
        <Typography
          variant="h5"
          textAlign={'center'}
          zIndex={1}
          sx={{ fontWeight: '600', lineHeight: '1.5' }}
        >
          Chào mừng bạn đến với <br></br> INDOCHINA PLAZA HANOI
        </Typography>
        <Stack className={cx('bottom__drawer')}>
          <MmenuButton>Bắt đầu</MmenuButton>
        </Stack>
      </Stack>
    </Stack>
  );
};

export default IntroComponents;
