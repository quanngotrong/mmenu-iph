import { TextField } from '@mui/material';
import { styled } from '@mui/material/styles';

const CustomizedField = styled(TextField)({
  width: '100%',
  fontSize: '0.875rem',
  borderRadius: '8px',
  marginBottom: '16px',
  marginTop: '8px',
  id: 'outlined-basic',
  variant: 'outlined',
});

export default CustomizedField;
