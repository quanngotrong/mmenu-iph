import classNames from 'classnames/bind';
import { Stack, Typography } from '@mui/material';
import styles from './MmenuToast.module.css';

const cx = classNames.bind(styles);

// eslint-disable-next-line unused-imports/no-unused-vars, no-unused-vars
const ToastComponents = ({ message, messageKey }) => {
  // const { t } = useTranslation(['common']);
  // if (!message && messageKey) {
  //     message = t(messageKey)
  // }
  return (
    <Stack className={cx('toast__message')} padding={1} alignItems={'center'}>
      <Typography
        variant="h6"
        bgcolor={'black'}
        width="fit-content"
        borderRadius={20}
        fontSize={16}
        fontWeight={500}
        textAlign={'center'}
        sx={{
          padding: '12px 32px',
        }}
      >
        {message || 'Email không chính xác'}
      </Typography>
    </Stack>
  );
};

export default ToastComponents;
