import { Dialog, DialogTitle, Stack } from '@mui/material';
import React from 'react';
import CustomizedField from '../CustomerField';
import MmenuButton from '../MmenuButton';
import MmenuWhiteButton from '../MmenuWhiteButton';

const DialogCreateArea = ({ open, handleCreateStore, handleOnClose }) => {
  const [area, setArea] = React.useState('');

  const handleChange = (event) => {
    setArea(event.target.value);
  };
  return (
    <Dialog
      open={open}
      onClose={handleOnClose}
      maxWidth={'xs'}
      fullWidth
      PaperProps={{
        maxHeight: '80vh',
      }}
    >
      <DialogTitle textAlign={'center'} fontSize={20} fontWeight={700}>
        Thêm mới gian hàng
      </DialogTitle>
      <Stack alignItems={'center'} p={2} spacing={2}>
        <CustomizedField label="Nhập tên khu vực" type="text"></CustomizedField>
        <Stack direction={'row'} width={1} spacing={2}>
          <MmenuWhiteButton
            title={'Đóng'}
            onClick={handleOnClose}
          ></MmenuWhiteButton>
          <MmenuButton onClick={handleCreateStore}>Thêm mới</MmenuButton>
        </Stack>
      </Stack>
    </Dialog>
  );
};

export default DialogCreateArea;
