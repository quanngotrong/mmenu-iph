import {
  Stack,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Typography,
} from '@mui/material';
import React, { useState } from 'react';
import { ControlPoint, QrCode } from '@mui/icons-material';
import MmenuButton from '../MmenuButton';
import MmenuWhiteButton from '../MmenuWhiteButton';
import DialogCreateStore from './DialogCreateStore';

const StoreTab = () => {
  const [area, setArea] = React.useState('');
  const [openDialogCreateStore, setOpenDialogCreateStore] = useState(false);

  const handleChange = (event) => {
    setArea(event.target.value);
  };
  return (
    <Stack spacing={2}>
      <FormControl fullWidth>
        <InputLabel id="demo-simple-select-label">Khu vực</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={area}
          label="Khu vực"
          onChange={handleChange}
        >
          <MenuItem value={10}>Tầng 1</MenuItem>
          <MenuItem value={20}>Tầng 2</MenuItem>
          <MenuItem value={30}>Tằng 3</MenuItem>
        </Select>
      </FormControl>
      <MmenuButton
        onClick={() => setOpenDialogCreateStore(true)}
        startIcon={<ControlPoint />}
      >
        Tạo gian hàng mới
      </MmenuButton>
      <Stack spacing={1}>
        <Typography>TẦNG 1</Typography>
        <MmenuWhiteButton
          title={'Gian hàng 1'}
          props={{
            startIcon: <ControlPoint />,
            sx: { justifyContent: 'start' },
          }}
        >
          <QrCode />
        </MmenuWhiteButton>
        <MmenuWhiteButton
          title={'Gian hàng 1'}
          props={{
            startIcon: <ControlPoint />,
            sx: { justifyContent: 'start' },
          }}
        >
          <QrCode />
        </MmenuWhiteButton>
        <MmenuWhiteButton
          title={'Gian hàng 1'}
          props={{
            startIcon: <ControlPoint />,
            sx: { justifyContent: 'start' },
          }}
        >
          <QrCode />
        </MmenuWhiteButton>
      </Stack>
      <DialogCreateStore
        open={openDialogCreateStore}
        handleOnClose={() => setOpenDialogCreateStore(false)}
      ></DialogCreateStore>
    </Stack>
  );
};

export default StoreTab;
