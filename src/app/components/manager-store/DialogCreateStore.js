import {
  Dialog,
  DialogTitle,
  Stack,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
} from '@mui/material';
import React from 'react';
import Image from 'next/image';
import { PhotoCamera } from '@mui/icons-material';
import CustomizedField from '../CustomerField';
import MmenuButton from '../MmenuButton';
import MmenuWhiteButton from '../MmenuWhiteButton';

const DialogCreateStore = ({ open, handleCreateStore, handleOnClose }) => {
  const [area, setArea] = React.useState('');

  const handleChange = (event) => {
    setArea(event.target.value);
  };
  return (
    <Dialog
      open={open}
      onClose={handleOnClose}
      maxWidth={'xs'}
      fullWidth
      PaperProps={{
        maxHeight: '80vh',
      }}
    >
      <DialogTitle textAlign={'center'} fontSize={20} fontWeight={700}>
        Thêm mới gian hàng
      </DialogTitle>
      <Stack alignItems={'center'} p={2} spacing={2}>
        <Stack position={'relative'} mb={2}>
          <Image
            src={'/store.png'}
            width={120}
            height={120}
            alt=""
            className="restaurant-image-upload"
          ></Image>
          <Stack
            p={1}
            bgcolor={'black'}
            position={'absolute'}
            borderRadius={'50%'}
            bottom={-5}
            right={-5}
            sx={{
              cursor: 'pointer',
            }}
            component={'label'}
          >
            <PhotoCamera
              sx={{ color: 'white', width: '24px', height: '24px' }}
            ></PhotoCamera>
            <input hidden accept="image/*" multiple type="file" />
          </Stack>
        </Stack>
        <CustomizedField
          label="Nhập mã gian hàng"
          type="text"
        ></CustomizedField>
        <CustomizedField
          label="Nhập mã gian hàng"
          type="text"
        ></CustomizedField>
        <FormControl fullWidth>
          <InputLabel id="demo-simple-select-label">Khu vực</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={area}
            label="Khu vực"
            onChange={handleChange}
          >
            <MenuItem value={10}>Tầng 1</MenuItem>
            <MenuItem value={20}>Tầng 2</MenuItem>
            <MenuItem value={30}>Tằng 3</MenuItem>
          </Select>
        </FormControl>
        <Stack direction={'row'} width={1} spacing={2}>
          <MmenuWhiteButton
            title={'Đóng'}
            onClick={handleOnClose}
          ></MmenuWhiteButton>
          <MmenuButton onClick={handleCreateStore}>Thêm mới</MmenuButton>
        </Stack>
      </Stack>
    </Dialog>
  );
};

export default DialogCreateStore;
