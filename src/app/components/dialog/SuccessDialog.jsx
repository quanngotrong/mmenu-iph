import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
// eslint-disable-next-line import/no-extraneous-dependencies
import PropTypes from 'prop-types';
import Image from 'next/image';
import { Typography } from '@mui/material';
import * as React from 'react';

import styles from '../appBar/AppBar.module.css';

export default function SuccessDialog(props) {
  const { onClose, open } = props;
  const handleClose = () => {
    onClose();
  };

  return (
    <Dialog
      fullWidth={true}
      onClose={handleClose}
      open={open}
      PaperProps={{
        style: {
          borderRadius: '20px',
        },
      }}
      BackdropProps={{ style: { backgroundColor: 'rgba(0, 0, 0, 0.5)' } }}
    >
      <div
        style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          height: '100%',
          marginTop: '1rem',
        }}
      >
        <Image src={'/iph-logo.svg'} width={110} height={32} alt=""></Image>
      </div>

      <div className={styles.Dialog_content}>
        <DialogTitle
          variant="h7"
          className={styles.Dialog_title}
          sx={{
            pb: '0.5rem',
            pt: '0.5rem',
            textAlign: 'center',
            justifyContent: 'center',
            fontWeight: 'bold',
          }}
        >
          Gửi thông tin thành công!
        </DialogTitle>
        <Typography variant="body2" sx={{ textAlign: 'center' }}>
          Voucher giảm giá sẽ được gửi đến Zalo của bạn sau khi thông tin đăng
          ký được kiểu tra hợp lệ.
        </Typography>
      </div>
    </Dialog>
  );
}

SuccessDialog.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
};
