/* eslint-disable import/no-extraneous-dependencies */
import React, { useState } from 'react';
import _ from 'lodash';
import {
  Stack,
  Typography,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Button,
  IconButton,
  Avatar,
  Box,
} from '@mui/material';
import { DateField } from '@mui/x-date-pickers';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { PhotoCamera, HighlightOff } from '@mui/icons-material';
import { uploadInvoiceImage } from '@/app/api/invoiceApi';
import Appbar from '../appBar/AppBar';
import CustomizedField from '../CustomerField';
import MmenuButton from '../MmenuButton';
import SuccessDialog from '../dialog/SuccessDialog';

const PersonalInfoForm = () => {
  return (
    <Stack
      sx={{
        width: '100%',
        backgroundColor: '#fff',
      }}
    >
      <Appbar />
      <Stack
        sx={{
          width: '100%',
          padding: '1rem',
        }}
      >
        <Typography
          variant="h7"
          zIndex={1}
          sx={{
            color: 'black',
            fontWeight: '600',
            lineHeight: '1.5',
            marginBottom: '10px',
          }}
        >
          Thông tin cá nhân
        </Typography>
        <CustomizedField
          type="text"
          label="Họ và tên"
          startAdornment={''}
          name="userName"
        />
        <CustomizedField
          type="tel"
          label="Số điện thoại"
          startAdornment={''}
          name="userPhoneNumber"
        />
        <CustomizedField
          type="email"
          label="Email"
          startAdornment={''}
          name="userEmail"
        />
        <CustomizedField
          type="text"
          label="Địa chỉ"
          startAdornment={''}
          name="userAddress"
        />
        <Stack
          sx={{
            width: '100%',
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'flex-start',
            justifyContent: 'space-between',
          }}
        >
          <LocalizationProvider dateAdapter={AdapterDayjs}>
            <DateField
              label="Ngày sinh"
              sx={{
                marginTop: '8px',
              }}
            ></DateField>
          </LocalizationProvider>
          <FormControl
            fullWidth
            sx={{
              margin: '8px 0 0 12px',
              maxWidth: '150px',
            }}
          >
            <InputLabel id="demo-simple-select-label">Giới tính</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              label="Age"
            >
              <MenuItem value={'male'}>Nam</MenuItem>
              <MenuItem value={'female'}>Nữ</MenuItem>
              <MenuItem value={'orther'}>Khác</MenuItem>
            </Select>
          </FormControl>
        </Stack>
        <BillInforForm />
      </Stack>
    </Stack>
  );
};

export default PersonalInfoForm;

const BillInforForm = () => {
  const [open, setOpen] = React.useState(false);
  const inputRef = React.useRef(null);
  const [imageUrls, setImageUrls] = useState([]);

  const handleInputChange = async (event) => {
    const { files } = event.target;

    try {
      const formData = new FormData();
      _.forEach(files, (file) => {
        formData.append('imageFiles', file);
      });
      // TODO: fix mallId, storeId
      const response = await uploadInvoiceImage({
        formData,
        mallId: 'abc',
        storeId: 'xyz',
      });

      if (_.get(response, 'results.length') > 0) {
        setImageUrls([...imageUrls, _.get(response, 'results')]);
      }
    } catch (error) {
      console.error('Error uploading file:', error);
    }
  };
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  return (
    <Stack
      sx={{
        width: '100%',
        backgroundColor: '#fff',
      }}
    >
      <Stack
        sx={{
          width: '100%',
          paddingTop: '1rem',
        }}
      >
        <Typography
          variant="h7"
          zIndex={1}
          sx={{
            color: 'black',
            fontWeight: '600',
            lineHeight: '1.5',
            marginBottom: '10px',
          }}
        >
          Thông tin hóa đơn
        </Typography>
        <CustomizedField
          type="text"
          label="PHÚC LONG COFEE & TEA"
          startAdornment={''}
          InputProps={{
            readOnly: true,
          }}
          name="userName"
        />
        <CustomizedField
          type="number"
          label="Số hóa đơn"
          startAdornment={''}
          name="userBillNumber"
        />
        <CustomizedField
          type="number"
          label="Giá trị hóa đơn"
          startAdornment={''}
          name="userBillCost"
        />
        <Button
          sx={{
            textTransform: 'none',
            width: '100%',
            borderRadius: '8px',
            border: '1px solid #c6c6c6',
            marginBottom: '16px',
            display: 'flex',
            justifyContent: 'space-between',
          }}
        >
          <Typography color={'#777'} fontWeight={400} variant="subtitle1">
            Tải ảnh hóa đơn
          </Typography>
          <IconButton
            color="primary"
            aria-label="upload picture"
            component="label"
            sx={{
              color: 'black',
            }}
          >
            <input
              hidden
              accept="image/*"
              type="file"
              onChange={handleInputChange}
            />
            <PhotoCamera />
          </IconButton>
        </Button>
        <Stack direction={'row'} marginBottom={'15px'} spacing={1}>
          {_.map(imageUrls, (url, index) => (
            <Box
              sx={{
                position: 'relative',
              }}
            >
              <Avatar key={index} src={url} variant="rounded"></Avatar>
              <IconButton
                sx={{
                  position: 'absolute',
                  top: '-1.1rem',
                  right: '-1.1rem',
                  color: 'black',
                }}
              >
                <HighlightOff />
              </IconButton>
            </Box>
          ))}
          <IconButton
            color="primary"
            aria-label="upload picture"
            sx={{
              margin: '0',
              padding: '0',
            }}
            component="label"
          >
            <input
              type="file"
              accept="image/*"
              onChange={handleInputChange}
              ref={inputRef}
              style={{ display: 'none' }}
            />
            <Avatar src="/plus.svg" variant="rounded"></Avatar>
          </IconButton>
        </Stack>
        <MmenuButton onClick={handleClickOpen}>Tiếp theo</MmenuButton>
        <SuccessDialog open={open} onClose={handleClose} />
      </Stack>
    </Stack>
  );
};
