import { useRouter } from 'next/router';
// eslint-disable-next-line import/no-cycle
import { _setState } from '../mallContext';

const _handleDirect = ({ destination, query }) => {
  const router = useRouter();
  if (destination) {
    router.push({
      pathname: destination,
      query,
    });
  }
};

export const redirectToAdminHome = ({ state }) => {
  _handleDirect({
    destination: 'admin-home-page',
  });
  return _setState({
    ...state,
  });
};

export const redirectToAdminStore = ({ state }) => {
  _handleDirect({
    destination: 'admin-store-page',
  });
  return _setState({
    ...state,
  });
};

export const redirectToAdminCustomer = ({ state }) => {
  _handleDirect({
    destination: 'admin-customer-page',
  });
  return _setState({
    ...state,
  });
};

export const redirectToAdminPromotion = ({ state }) => {
  _handleDirect({
    destination: 'admin-promotion-page',
  });
  return _setState({
    ...state,
  });
};
