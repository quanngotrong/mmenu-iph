import _ from 'lodash';
import { setUserInfo, setJWTToken } from '@/app/service/localStorage';
import { loginUser } from '@/app/api/userApi';
import { DirectContant } from '@/app/utils/contants';

export const loginUserAction = async ({ loginUserBody, dispatch }) => {
  try {
    const loginResponse = await loginUser(loginUserBody);
    const userInfor = _.get(loginResponse, 'user');
    const token = _.get(loginResponse, 'accessToken.token');
    setUserInfo(userInfor);
    setJWTToken(token);
    dispatch({
      type: DirectContant.DIRECT_TO_ADMIN_HOME,
      payload: {},
    });
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error);
  }
};
