import { createContext, useReducer } from 'react';
import { DirectContant, UserContants } from '../utils/contants';
import { loginUserAction } from './action/userAction';
// eslint-disable-next-line import/no-cycle
import {
  redirectToAdminCustomer,
  redirectToAdminHome,
  redirectToAdminStore,
} from './action/directAction';

export const StoreContext = createContext('');

const initialState = {};

export const _setState = (state, payload) => {
  return {
    ...state,
    ...payload,
  };
};

const reducer = (state, action) => {
  switch (action.type) {
    case UserContants.LOGIN:
      loginUserAction({
        loginUserBody: action.payload.loginUserBody,
        dispatch: action.payload.dispatch,
      });
      return _setState({ ...state });
    case DirectContant.DIRECT_TO_ADMIN_HOME:
      return redirectToAdminHome({ state });
    case DirectContant.DIRECT_TO_ADMIN_STORE:
      return redirectToAdminStore({ state });
    case DirectContant.DIRECT_TO_ADMIN_CUSTOMER:
      return redirectToAdminCustomer({ state });
    default:
      return _setState({ ...state });
  }
};

export const StoreProvider = (props) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <StoreContext.Provider value={{ state, dispatch }}>
      {props.children}
    </StoreContext.Provider>
  );
};
