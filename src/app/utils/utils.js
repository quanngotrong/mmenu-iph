import { useRouter } from 'next/router';

export const useBackButton = () => {
  const router = useRouter();
  const handleBack = () => {
    router.back();
  };
  return handleBack;
};
