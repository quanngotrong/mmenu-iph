import ErrorComponents from '@/app/components/error/Error';

export default function Error() {
  return (
    <>
      <ErrorComponents></ErrorComponents>
    </>
  );
}
