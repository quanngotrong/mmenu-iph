import React from 'react';
import { Stack, Box, Tabs, Tab } from '@mui/material';
import { TabContext, TabPanel } from '@mui/lab';
import BackHeader from '@/app/components/BackHeader';
import AreaTab from '@/app/components/manager-store/Area';
import EmployeeTab from '@/app/components/manager-employee/EmployeeTab';
import DepartmentTab from '@/app/components/manager-employee/DepartmentTab';

const Employee = () => {
  const [value, setValue] = React.useState('1');

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  return (
    <Stack
      bgcolor={'white'}
      minHeight={'100vh'}
      color={'black'}
      alignItems={'center'}
    >
      <Stack
        maxWidth={'440px'}
        width={1}
        alignItems={'center'}
        border={'1px solid #D5D9E2'}
        minHeight={'100vh'}
        display={'block'}
      >
        <BackHeader displayText={'Quản lý nhân viên'}></BackHeader>
        <Stack width={1}>
          <TabContext value={value}>
            <Box sx={{ width: '100%', backgroundColor: '#fff' }}>
              <Tabs
                value={value}
                onChange={handleChange}
                aria-label="secondary tabs example"
                variant="fullWidth"
                indicatorColor="transparent"
                textColor="#000"
                centered
                sx={{
                  '.Mui-selected': {
                    backgroundColor: '#000',
                    color: '#fff',
                  },
                  borderTop: '1px solid #E2E2E2',
                  borderBottom: '1px solid #E2E2E2',
                }}
              >
                <Tab value="1" label="NHÂN VIÊN" />
                <Tab value="2" label="BỘ PHẬN" />
              </Tabs>
            </Box>
            <TabPanel value="1" sx={{ p: 0 }}>
              <EmployeeTab></EmployeeTab>
            </TabPanel>
            <TabPanel value="2">
              <DepartmentTab></DepartmentTab>
            </TabPanel>
          </TabContext>
        </Stack>
      </Stack>
    </Stack>
  );
};

export default Employee;
