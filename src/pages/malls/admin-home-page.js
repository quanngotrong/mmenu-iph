import { Stack, Typography } from '@mui/material';
import Image from 'next/image';
import AdminHome from '@/app/components/admin-home/AdminHome';

export default function AdminHomePage() {
  return (
    <Stack bgcolor={'white'} minHeight={'100vh'}>
      <AdminHome></AdminHome>
      <Stack
        direction={'row'}
        justifyContent={'space-between'}
        width={'-webkit-fill-available'}
        padding={'16px'}
        position={'absolute'}
        bottom={'0'}
      >
        <Typography variant="caption" fontWeight={600} color={'black'}>
          Indochina Plaza Hanoi<br></br> 241 Xuân Thủy, Cầu Giấy, Hà Nội
        </Typography>
        <Image
          src={'/power-by-mmenu.svg'}
          width={86}
          height={28}
          alt=""
        ></Image>
      </Stack>
    </Stack>
  );
}
