import { Stack } from '@mui/material';
import Appbar from '@/app/components/appBar/AppBar';
import CustomizeTabs from '../../app/components/tab/Tabs';

export default function ManagerHomePage() {
  return (
    <Stack width="100%" height="100vh" bgcolor="#fff" color={'black'}>
      <Appbar></Appbar>
      <CustomizeTabs />
    </Stack>
  );
}
